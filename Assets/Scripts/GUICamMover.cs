﻿using UnityEngine;
using System.Collections;

public class GUICamMover : MonoBehaviour {
	public static GUICamMover staticRef;
	public Camera cam;
	// Use this for initialization
	void Start () {
		staticRef = this;
		this.cam = transform.GetComponent<Camera> ();
	}
}
