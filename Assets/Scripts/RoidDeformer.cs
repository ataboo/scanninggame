﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (MeshFilter))]
public class RoidDeformer : MonoBehaviour {
	public GameObject craterMaker;
	public float craterRadius = 0.75f;

	private float radSquare;
	private MeshFilter meshFilter;
	// Use this for initialization
	void Start () {
		this.meshFilter = transform.GetComponent<MeshFilter> ();


	}
	
	// Update is called once per frame
	void Update () {
		radSquare = craterRadius * craterRadius;
		Mesh sphereMesh = meshFilter.mesh;
		Vector3[] points = sphereMesh.vertices;
		Vector3 craterMakerPos = craterMaker.transform.localPosition;

		for(int i=0; i<points.Length; i++){
			Vector3 pointBefore = points[i];
			Vector3 deltaPos = points[i] - craterMakerPos;
			float squarePos = deltaPos.sqrMagnitude;
			//float magPos = deltaPos.magnitude;
			//Debug.Log (string.Format("squarePos: {0}, radSquare {1}, magPos {2}", squarePos, radSquare, magPos));
			if(squarePos > radSquare) continue;
			points[i] = deltaPos.normalized * craterRadius + craterMakerPos;
			Vector3 pointAfter = points[i];
			Debug.Log (string.Format("squarePos: {0}, radSquare {1}, pointBefore {2}, pointAfter {3}", squarePos, radSquare, pointBefore.x, pointAfter.x));
		}
		sphereMesh.vertices = points;
		meshFilter.mesh = sphereMesh;

	}
}
