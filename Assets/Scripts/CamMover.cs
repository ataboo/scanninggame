﻿using UnityEngine;
using System.Collections;

public class CamMover : MonoBehaviour {
	
	public static CamMover staticRef;
	public static float zoomSpeed = 1f;
	private float camMax = 10000f;
	private float camMin = 15f;

	public GameObject backgroundPlane;

	public static float camSmoothing = 8f;
	private Vector3 camLag = new Vector3(0,0);
	private Camera perspectiveCam;
	private Camera guiCam;

	float zoom = 1200f;

	// Use this for initialization
	void Start () {
		staticRef = this;
		perspectiveCam = transform.GetComponentInChildren<Camera> ();
	}
	//Called by stage manager
	public void UpdateCamera(GameObject targetObj, bool snapMove, float maxZoom){
		this.camMin = maxZoom;
		if (guiCam == null) {
			this.guiCam = GUICamMover.staticRef.cam;
		}
		if (targetObj == null) {
			Debug.Log ("updateCamera: target object null");
			return;
		}
		Transform target = targetObj.transform;

		zoom = Mathf.Clamp(zoom - Input.GetAxis("Mouse ScrollWheel") * zoomSpeed * zoom, camMin, camMax);
		perspectiveCam.orthographicSize = zoom;
		guiCam.orthographicSize = zoom;
		Vector3 backgroundSize = new Vector3 (zoom / 5 * 16 / 9, 0, zoom / 5);
		backgroundPlane.transform.localScale = backgroundSize;
		if (target == null)
			return;

		Vector3 targetPos = new Vector3 (target.position.x, target.position.y, transform.position.z);
		if (snapMove) {
			//Smooths lerp
			transform.position = targetPos + camLag;
		} else {
			transform.position = Vector3.Lerp (transform.position, targetPos, Time.deltaTime * camSmoothing);
		}

		this.camLag = transform.position - targetPos;
	}


}

