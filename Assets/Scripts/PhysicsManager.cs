using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class PhysicsManager : MonoBehaviour {
	
	public const float movementThreshold = 0.0001f;  //when values are close enough for movement
	//Frame rate maybe?

	public static PhysicsManager staticRef;
	private StageManager stageMan;

	//public Vessel playerShip;
	private int masterSerialCount = 0;
	private Dictionary<int, SpaceObject> activeObjects;
	private List<int> portSpawnSerials = new List<int> ();
	private static bool securityCheck = true;

	public float gameClock = 0;
	
	// Use this for initialization
	void Start () {
		staticRef = this;

		masterSerialCount = 0;
		this.activeObjects = new Dictionary<int, SpaceObject>();


	}
	
	void FixedUpdate() {
		//Debug.Log ("Started physManFixed: " + Time.realtimeSinceStartup);
		if (stageMan == null) {
			this.stageMan = StageManager.staticRef;
			//AddObjectsFromEmpties();
			NewGame();
			//SerializeTests();



		}
		//for each vessel
		List<int> objKeys= new List<int> (activeObjects.Keys);
		foreach (int key in objKeys) {
			SpaceObject spaceObj = activeObjects[key];
			UpdateVisibleSerials(spaceObj);
			//updateAI waypoints/intents
			//updateAI steering/weapons
			if(spaceObj is Vessel){
				//Debug.Log (Time.deltaTime);
				((Vessel)spaceObj).UpdateMovement();
				continue;
			}
			if(spaceObj is PortSpawner){
				//This can be on a longer fixed update later
				PortSpawner pSpawn = (PortSpawner) spaceObj;
				if(pSpawn.spawnUpdate()){
					Debug.Log (string.Format("{0} spawned something.", pSpawn.name));
				};
			}
		}

		RunCollisionChecks ();

		gameClock += Time.deltaTime;

		//UpdateVisibleSerials(playerShip);
		//resolveCollisions, etc
		//Debug.Log ("Finished physManFixed: " + Time.realtimeSinceStartup);
	}

	private void SerializeTests(){
		SpaceObject serSpace = new SpaceObject();
		PortSpawner serPort = ConfigParser.MakePortFromConfig ("SerTest", new DoubleVect2 (100, 100), 90);
		Vessel serVessel = ConfigParser.MakeVesselFromConfig (ConfigParser.SpaceObjectTypes.Submarine, "SerVessel", new DoubleVect2(200,200));
		FileSerializer.BinSaveToFile("SpaceObject", serSpace);
		FileSerializer.BinSaveToFile ("PortSpawner", serPort);
		FileSerializer.BinSaveToFile ("SerVessel", serVessel);
		Debug.Log ("PhysicsManager: serialized testObject");
		
		//SpaceObject loadedObject = (SpaceObject) BinarySerializer.LoadFromPrefs("TestObject");
		Debug.Log("PhysicsManager: loaded testObject");
		
	}

	private Vessel playerShip;
	private Vessel secondShip;
	private Vessel portCol;
	private void AddInitialVessels(){

		//Innitialize world from template
		//this.playerShip = new Vessel(Vessel.VesselTypes.Submarine, "HMS Playerface", new DoubleVect2(1000,1000));
		//AddSpaceObject (playerShip);
		masterSerialCount = 0;

		int[,] portSpots = new int[,]{{2000, 0, 270}, {0, 2000, 180}, {-3000, 0, 90}};
		for (int i=0; i < portSpots.GetLength(0); i++) {
			PortSpawner pSpawn = (PortSpawner) ConfigParser.MakePortFromConfig(
					"Port " + i, new DoubleVect2(portSpots[i,0], portSpots[i,1]), portSpots[i,2]);
			AddSpaceObject(pSpawn);
		}


		/*
		this.playerShip = ConfigParser.MakeVesselFromConfig(ConfigParser.VesselTypes.Submarine, "HMS Playerface", new DoubleVect2(1000,1000));
		AddSpaceObject (playerShip);

		this.secondShip = ConfigParser.MakeVesselFromConfig (ConfigParser.VesselTypes.CargoShip, "HMS Ramming Speed", new DoubleVect2 (1200, 1200));
		AddSpaceObject (secondShip);

		this.portCol = ConfigParser.MakeVesselFromConfig (ConfigParser.VesselTypes.PortCol, "Port Collision Tester", new DoubleVect2 (800, 1000));
		AddSpaceObject (portCol);
		*/
	}


	private void AddObjectsFromEmpties(){
		GameObject[] portSpawns = GameObject.FindGameObjectsWithTag ("port_spawn");
		foreach (GameObject spawnEmpty in portSpawns) {
			PortSpawner pSpawn = ConfigParser.MakePortFromConfig(spawnEmpty.name, 
					stageMan.GridToWorld(spawnEmpty.transform.position), spawnEmpty.transform.rotation.eulerAngles.z);
			AddSpaceObject(pSpawn);
		}
	}

	private void RunCollisionChecks(){
		int[] serials = new int[activeObjects.Count];
		activeObjects.Keys.CopyTo(serials, 0);

		stageMan.ResetCollideColours ();
		for(int i=0; i<serials.Length; i++){
			for(int j=i+1; j<serials.Length; j++){
				bool collided = CollisionTester.RunSAT(activeObjects[serials[i]], activeObjects[serials[j]]);
				if(collided){
					Debug.Log ("Collided");
					stageMan.SetObjectColour(activeObjects[serials[i]].serialNumber, Color.red);
					stageMan.SetObjectColour(activeObjects[serials[j]].serialNumber, Color.red);
				}
			}
		}
	}

	public void AddSpaceObject(SpaceObject spaceObj){
		spaceObj.serialNumber = masterSerialCount;
		activeObjects.Add (masterSerialCount, spaceObj);
		if (spaceObj is PortSpawner) {
			portSpawnSerials.Add(spaceObj.serialNumber);
		}
		Debug.Log ("Added serial number: " + masterSerialCount);
		masterSerialCount++;
	}

	//Used when loading from serialized
	private void AddSpaceObject(SpaceObject spaceObj, int serial){
		activeObjects.Add (serial, spaceObj);
		if (spaceObj is PortSpawner) {
			portSpawnSerials.Add(spaceObj.serialNumber);
		}
		if(masterSerialCount <= serial) masterSerialCount = serial + 1;
	}

	public void RemoveSpaceObject(SpaceObject spaceObj){
		activeObjects.Remove (spaceObj.serialNumber);
		if (spaceObj is PortSpawner) {
			portSpawnSerials.Remove(spaceObj.serialNumber);
		}

		if (stageMan.targetSpaceObject == spaceObj) {
			stageMan.GetNextTarget();
		}
	}

	public SpaceObject SpawnVessel(ConfigParser.SpaceObjectTypes type, DoubleVect2 location, float spawnHeading){
		SpaceObject newObject;

		//TODO: ship names

		switch (type) {
		case ConfigParser.SpaceObjectTypes.Submarine:
			newObject = ConfigParser.MakeVesselFromConfig(ConfigParser.SpaceObjectTypes.Submarine, "HMS Port Baby", location);
			break;       
		case ConfigParser.SpaceObjectTypes.CargoShip:
			newObject = ConfigParser.MakeVesselFromConfig(ConfigParser.SpaceObjectTypes.CargoShip, "HMS Wide Load", location);
			break;
		case ConfigParser.SpaceObjectTypes.Tanker:
			newObject = ConfigParser.MakeVesselFromConfig(ConfigParser.SpaceObjectTypes.Tanker, "HMS Hindenberg 2", location);
			break;
		case ConfigParser.SpaceObjectTypes.PortSpawner:
			newObject = ConfigParser.MakePortFromConfig("Newport Station", location, spawnHeading);
			break;
		default:
			Debug.LogError("Invalid type for spawnVessel");
			return null;
		}
		newObject.heading = spawnHeading;

		AddSpaceObject (newObject);
		return newObject;
	}

	public void UpdateVisibleSerials(SpaceObject spaceObj){
		//ArrayList vesselsInRange = new ArrayList ();
		//cull far away
		//use stats to determine visibility
		spaceObj.ClearSerials();
		foreach(KeyValuePair<int, SpaceObject> entry in activeObjects){
			//if logic
			SpaceObject targObj = entry.Value;
			//if(vessel.serialNumber == ves.serialNumber) continue;
			spaceObj.detectableSerials.Add (targObj.serialNumber);
		}
	}

	public void UpdateVisibleSerials(int serial){
		SpaceObject spaceObj = activeObjects [serial];
		if (spaceObj != null) {
			UpdateVisibleSerials (spaceObj);
		} else {
			Debug.LogError(string.Format("UpdateVisibleSerials failed to find serial: {0}.", serial)); 
		}
	}

	public SpaceObject GetObjectBySerial(int serial, int detectorObjectSerial){
		if (securityCheck) {
			bool isVis = false;
			SpaceObject detectObject = activeObjects[detectorObjectSerial];
			if(detectObject == null){
				Debug.LogError("Failed getVesselBySerial: couldn't find detectorVesselSerial.");
				return null;
			}
			foreach(int ser in detectObject.detectableSerials){
				if(serial == ser){
					isVis = true;
					break;
				}

			}
			if(!isVis){
				Debug.Log(string.Format("{0} (Serial: {1}) should not be asking for serial {2} because it's not detectable.", 
						detectObject.name, detectObject.serialNumber, serial));
				return null;
			}
		}
		return GetObjectBySerial (serial);

	}

	public SpaceObject GetObjectBySerial(int serial){
		if (activeObjects.ContainsKey (serial)) {
			return activeObjects [serial];
		} else {
			Debug.LogError("Serial number: " + serial + " not found in activeVessels");
			return null;
		}
	}

	public int[] ConeScan(int scanSerial){
		List<int> scannedSerials = new List<int> ();

		SpaceObject scanObject = activeObjects [scanSerial];

		//Debug.Log ("scanObject is vessel " + scanObject.serialNumber + ": " + scanObject is Vessel + " scanObject is null: " + scanObject == null);
		if (scanObject == null || !(scanObject is Vessel)) {
			//Debug.LogError("coneScan can't find serial: " + scanSerial + " or it's not a vessel.");
			int[] retArr = new int[]{};
			return retArr;
		}
		Vessel scanVessel = (Vessel) scanObject;
		scanVessel.UpdateScanRange ();

		foreach (KeyValuePair<int, SpaceObject> pair in activeObjects) {
			SpaceObject targObj = pair.Value;
			if(targObj == scanVessel)
				continue;

			DoubleVect2 deltaPos = targObj.position - scanVessel.position;
			if(deltaPos.MagSqr() > scanVessel.scanRange * scanVessel.scanRange){
				continue;
			}
			float deltaEuler = MathUtils.getSteerBearing(scanVessel.scanConeState.x, deltaPos.EulerAngle());
			if(Mathf.Abs(deltaEuler) < (scanVessel.scanConeState.y / 2)){
				scannedSerials.Add(targObj.serialNumber);
			}
		}

		return scannedSerials.ToArray();

		
	}

	public int[] GetNeighbourPorts(int sourceSerial){
		//If requested by a port, it won't return itself
		int portCount = portSpawnSerials.Contains (sourceSerial) ? portSpawnSerials.Count - 1 : portSpawnSerials.Count; 

		//Can be some range logic later
		int[] neighbours = new int[portCount];
		int count = 0;
		foreach (int i in portSpawnSerials) {
			if(i == sourceSerial) continue;
			neighbours[count] = i;
			count++;
		}
		return neighbours;
	}

	public PortSpawner GetPort(int serial){
		SpaceObject spaceObj = activeObjects [serial];
		if (spaceObj is PortSpawner) {
			return (PortSpawner) spaceObj;
		}
		Debug.Log ("getPortLocation: " + serial + " failed. spaceObj is not PortSpawner or null.");
		return null;
	}

	public SpaceObject GetNextSpaceObject(int currentSerial){
		int[] activeKeys = new int[activeObjects.Keys.Count];
		activeObjects.Keys.CopyTo(activeKeys, 0);
		if (activeKeys.Length == 0) {
			Debug.Log ("No Space Objects yet");
			return null;
		}
		int nextSerial = activeKeys[0];
		for (int i=0; i<activeKeys.Length; i++) {
			if(activeKeys[i] == currentSerial && i!=activeKeys.Length - 1){
				nextSerial = activeKeys[i+1];
			}
		}

		return GetObjectBySerial(nextSerial);

	}

	public void NewGame(){
		Debug.Log ("New Game");
		ClearObjects ();
		AddInitialVessels ();
	}

	public void SaveFile(string fileName){
		Debug.Log ("Save Game");
		SpaceObject[] activeObjArr = new SpaceObject[activeObjects.Count];
		activeObjects.Values.CopyTo(activeObjArr, 0);
		SaveFileManager.SaveActiveObjects (fileName, gameClock, activeObjArr);
	}

	public void LoadFile(string fileName){
		Debug.Log ("Load Save");
		ClearObjects();
		SpaceObject[] activeObjArr = SaveFileManager.LoadActiveObjects (fileName);
		foreach (SpaceObject spaceObj in activeObjArr) {
			AddSpaceObject(spaceObj, spaceObj.serialNumber);
		}
	}

	public void ClearObjects(){
		masterSerialCount = 0;
		gameClock = 0;
		stageMan.ClearStage ();
		int[] activeSerials = new int[activeObjects.Count];
		portSpawnSerials.Clear();
		activeObjects.Keys.CopyTo (activeSerials, 0);
		foreach (int serial in activeSerials) {
			RemoveSpaceObject(activeObjects[serial]);
		}
	}

	public void QuitGame(){
		SaveFile("autosave.xml");
		Application.Quit();
	}
	
}
