using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;

public class StageManager : MonoBehaviour {

	[HideInInspector]
	public DoubleVect2 gridCenter = new DoubleVect2 (0, 0);
	public static float[] maxRenderRange = {5000f, 500f}; //Range, buffer to prevent repetative reloading
	public const float gridRadius = 10000f;
	[HideInInspector]
	public SpaceObject targetSpaceObject = null;

	public const KeyCode stopKey = KeyCode.X;
	public static StageManager staticRef;
	private PhysicsManager physMan;
	private CamMover perCamMover;
	private GUIOverview guiControl;
	public static float closeEnough = PhysicsManager.movementThreshold;
	private bool snapCam = false;

	public bool scanModeOn = false;

	private Dictionary<int, SpaceObject> visibleSpaceObjects = new Dictionary<int, SpaceObject>();
	// Use this for initialization
	void Start () {
		staticRef = this;
		//going to need player mode
	}

	void FixedUpdate () {
		//Debug.Log ("Started stageManFixed at: " + Time.realtimeSinceStartup);

		//First Run
		if (physMan == null) {
			this.physMan = PhysicsManager.staticRef;
			this.perCamMover = CamMover.staticRef;
			this.guiControl = GUIOverview.staticRef;
			GetNextTarget(); //Needed to innitialize targetspaceobject as null
		}

		if (targetSpaceObject == null) {
			GetNextTarget ();
			if(targetSpaceObject == null){
				Debug.Log ("Still no target object");
				return;
			}
			Debug.Log("Target was null but is now serial: " + targetSpaceObject.serialNumber);
		}

		if (targetSpaceObject.serialNumber < 0) {
			GetNextTarget();
		}


		//Going to need player modes
		
		//if input change target build around new target


		UpdateDetectableList ();
		UpdateVesselRendering ();
		if (scanModeOn) {
			UpdateScanCone ();
		}


		//snapCam flagged true if BuildAroundTarget called or grid recentered in updateVisibleVessels
		perCamMover.UpdateCamera(targetSpaceObject.GetRenderObject(), snapCam, Mathf.Sqrt(targetSpaceObject.minColliderRadSqr));
		snapCam = false;
		//Debug.Log ("Finished stageManFixedat: " + Time.realtimeSinceStartup);
	}

	void Update(){
		UpdatePlayerInput ();
	}

	private void UpdateDetectableList(){
		//Remove vessels not found in detectable serials
		List<int> renderedKeys= new List<int> (visibleSpaceObjects.Keys);
		foreach (int serial in renderedKeys) {
			if(!targetSpaceObject.detectableSerials.Contains(serial)){
				RemoveObjectModel(visibleSpaceObjects[serial]);
				visibleSpaceObjects.Remove(serial);
			}
		}

		//Add vessels missing from renderedVessels
		foreach (int serial in targetSpaceObject.detectableSerials) {
			if(!visibleSpaceObjects.ContainsKey(serial)){
				SpaceObject addObj = physMan.GetObjectBySerial(serial, targetSpaceObject.serialNumber);
				if(addObj != null)
					visibleSpaceObjects.Add(addObj.serialNumber, addObj);
			}
		}
	}

	//TODO: move camera zoom input to here
	private void UpdatePlayerInput (){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			GUIOverview.staticRef.ToggleMenu();
		}

		if (GUIOverview.mainMenuOpen) {
			return;
		}

		//This will need to be better later

		if (Input.GetKeyDown (KeyCode.Tab)) {
			GetNextTarget();
		}

		if (!(targetSpaceObject is Vessel)) {
			scanModeOn = false;
			//Debug.Log ("targetesObject is not a vessel");
			return;
		}

		Vessel targetedVessel = (Vessel) targetSpaceObject;

		if(Input.GetMouseButtonDown(1)){
			NavWaypoint newWay = WaypointClick(); //... way
			if(Input.GetKey(KeyCode.LeftShift)){
				targetedVessel.TruncateWaypoint(newWay);
			} else {
				targetedVessel.SetDestination(newWay);
			}
		}

		if(Input.GetMouseButtonDown(0)){
			targetedVessel.FireWeapons();
		}

		/*
		if(Input.GetKeyUp(KeyCode.Keypad0)){
			Vector3 mousePos = Input.mousePosition;
			Vector3 spawnPos = Camera.main.ScreenToWorldPoint(mousePos);
			spawnPos.z = 0;
			Vessel spawnSub = new Submarine("HMS Manual Spawn", -1, GridToWorld(spawnPos));
			physMan.AddSpaceObject(spawnSub);
		}
		*/

		if (Input.GetKey (KeyCode.E)) {
			targetedVessel.RotateScanCone(1);
			scanModeOn = true;
		}

		if (Input.GetKey (KeyCode.Q)) {
			targetedVessel.RotateScanCone(-1);
			scanModeOn = true;
		}

		if (Input.GetKey (KeyCode.Z)) {
			targetedVessel.SpreadScanCone(-1);
			scanModeOn = true;
		}
		if (Input.GetKey (KeyCode.C)) {
			targetedVessel.SpreadScanCone(1);
			scanModeOn = true;
		}

		if(Input.GetKeyDown (KeyCode.Space)) {
			this.scanModeOn = !this.scanModeOn;
		}

		if (Mathf.Abs (Input.GetAxis ("Horizontal")) > 0 || Mathf.Abs (Input.GetAxis ("Vertical")) > 0) {
			targetedVessel.desiredHeading =  MathUtils.clampEuler(targetedVessel.desiredHeading + Input.GetAxis ("Horizontal") * targetedVessel.courseDelta * 10);

			targetedVessel.desiredSpeed = MathUtils.ThresholdClamp(Input.GetAxis ("Vertical") * targetedVessel.throttleDelta + targetedVessel.desiredSpeed, 
			                                                   targetedVessel.speedLimits.x, targetedVessel.speedLimits.y, closeEnough);

			targetedVessel.ClearWaypoints();
		}

		if (Input.GetKeyUp (stopKey)) {
			targetedVessel.desiredSpeed = 0;
			targetedVessel.ClearWaypoints();
		}

		if (ScanConeMesh.staticRef.isVisible != scanModeOn) {
			if(ScanConeMesh.staticRef.scanMesh == null){
				UpdateScanCone();
			}
			ScanConeMesh.staticRef.SetConeVisible(scanModeOn);
		}

	}

	private void BuildAroundTarget(SpaceObject spaceObj){
		snapCam = true;

		this.gridCenter = spaceObj.position;

		//add target at center
		AddObjectModel (spaceObj);
		this.targetSpaceObject = spaceObj;
		ClearAllWaypointLines ();
		physMan.UpdateVisibleSerials (spaceObj.serialNumber);
	}

	private void ClearAllWaypointLines(){
		foreach (KeyValuePair<int, SpaceObject> pair in visibleSpaceObjects) {
			if(!(pair.Value is Vessel)) continue;
			ClearDebugLines((Vessel) pair.Value);
		}
	}

	private void UpdateVesselRendering(){
		//Free camera down the road?
		if (targetSpaceObject == null) {
			return;
		}

		//Recenter grid to targetVessel if outside render range
		if (WorldToGrid (targetSpaceObject.position).sqrMagnitude > gridRadius * gridRadius) {
			this.gridCenter = targetSpaceObject.position;
			snapCam = true;
		}

		foreach (KeyValuePair<int, SpaceObject> pair in visibleSpaceObjects) {
			SpaceObject spaceObj = pair.Value;
			if((spaceObj.position - targetSpaceObject.position).MagSqr()
					> maxRenderRange[0] * maxRenderRange[0]){
				if(spaceObj.GetRenderObject() != null) 
					RemoveObjectModel(spaceObj);
				continue;
			}

			GameObject obj = spaceObj.GetRenderObject();
			if(obj == null){
				AddObjectModel(spaceObj);
				continue;
			}
			if(obj != null){
				obj.transform.position = WorldToGrid (spaceObj.position);
				obj.transform.rotation = Quaternion.Euler (0, 180, spaceObj.heading);

				if(spaceObj is Vessel){

					Vessel vessel = (Vessel) spaceObj;
					vessel.UpdateRenders();

					UpdateDebugLines(vessel);
				}
			}
		}



		//Debug.Log (string.Format("WorldPos: {0}, {1}, GridPos: {2}, {3}", targetVessel.position.x, targetVessel.position.y, 
		//                       worldToGrid(targetVessel.position).x, worldToGrid(targetVessel.position).y));
		//for each render them
	}

	public void GetNextTarget(){
		int serial = -1;
		if (targetSpaceObject != null)
			serial = targetSpaceObject.serialNumber;

		this.targetSpaceObject = physMan.GetNextSpaceObject (serial);
		if (targetSpaceObject != null) {
			BuildAroundTarget(targetSpaceObject);
		}
	}

	private void AddObjectModel(SpaceObject spaceObj){
		Debug.Log ("Adding model serial: " + spaceObj.serialNumber);
		//spaceObj.renderObject = (GameObject) Instantiate(spaceObj.prefab, WorldToGrid(spaceObj.position), Quaternion.Euler(0,180, spaceObj.heading));
		spaceObj.AddRenderObject ();
	}

	private void RemoveObjectModel(SpaceObject spaceObj){
		if (spaceObj.GetRenderObject() == null) {
			Debug.Log("RemoveObjectModel: tried to remove null object serial: " + spaceObj.serialNumber); 
			return;
		}
		Debug.Log ("Removing model serial: " + spaceObj.serialNumber);
		spaceObj.ClearRenderObject ();
		if(spaceObj is Vessel){
			ClearDebugLines((Vessel) spaceObj);
		}
	}

	private VectorLine desSpeedLine;
	//private VectorLine deltaVLine;
	//private VectorLine thrustLine;
	//private VectorLine headingLine;
	private VectorLine centerXLine;
	private VectorLine centerYLine;

	//private VectorLine gridRadiusCirc;
	private VectorLine renderRangeCirc;
	private VectorLine scanRadiusCirc;

	public void UpdateDebugLines(Vessel vessel, DoubleVect2 desiredSpeedVect, DoubleVect2 deltaVBore, DoubleVect2 thrustVect){

		desSpeedLine = DrawVectrocity (desSpeedLine, WorldToGrid(vessel.position), WorldToGrid(desiredSpeedVect + vessel.position), Color.red);
		desSpeedLine.name = "Desired Speed";
		desSpeedLine.Draw();

		/*
		DoubleVect2 deltaVOffset = vessel.position.addVect(new DoubleVect2 (100, 0)); 
		deltaVLine = drawVectrocity(deltaVLine, worldToGrid(deltaVOffset), worldToGrid(deltaVBore.addVect(deltaVOffset)), Color.blue);
		deltaVLine.Draw();                       


		thrustLine = drawVectrocity (thrustLine, worldToGrid (vessel.position), worldToGrid (thrustVect.multiply (6).addVect (vessel.position)), Color.red);
		thrustLine.Draw ();

		headingLine = drawVectrocity (headingLine, worldToGrid (deltaVOffset), 
				worldToGrid (new DoubleVect2 (0, 40).rotate (-vessel.desiredHeading).addVect (deltaVOffset)), Color.yellow);
		headingLine.Draw ();
		*/


		renderRangeCirc = DrawCircleVec (renderRangeCirc, "Render Range", WorldToGrid (vessel.position), maxRenderRange[0], Color.yellow);
		renderRangeCirc.Draw3D ();

		//gridRadiusCirc = drawCircleVec (gridRadiusCirc, "Grid Radius", new Vector2 (0, 0), gridRadius, Color.red);
		//gridRadiusCirc.Draw ();

		/*
		Color scanRadColour = scanModeOn ? Color.blue: (Color) new Color32(0,0,0,0);
		scanRadiusCirc = DrawCircleVec (scanRadiusCirc, "Scan Radius", WorldToGrid (vessel.position), vessel.scanRange, scanRadColour);
		scanRadiusCirc.Draw3D ();
		*/

		float xLength = 20;
		centerXLine = DrawVectrocity (centerXLine, new Vector2(-xLength, 0),
		                              new Vector2(xLength, 0), Color.red);
		centerXLine.Draw3D ();
		centerYLine = DrawVectrocity (centerYLine, new Vector2(0, -xLength),
		        new Vector2(0, xLength), Color.red);
		centerYLine.Draw3D ();

	}

	public VectorLine DrawCircleVec(VectorLine line, string name, Vector2 center, float radius, Color colour){
		int circleRes = 64;
		Vector3 circCenter = new Vector3(center.x, center.y, 0);
		if (line == null) {
			line = new VectorLine (name, new Vector3[circleRes], null, 2.0f, LineType.Continuous);
		}
		line.MakeCircle (circCenter, Vector3.forward, radius);
		line.color = colour;
		return line;
	}

	public VectorLine DrawVectrocity (VectorLine line, Vector2 start, Vector2 end, Color colour){
		Vector3 start3 = new Vector3(start.x, start.y, -5);
		Vector3 end3 = new Vector3(end.x, end.y, -5);
		if (line == null) {
			line = new VectorLine("drawVec", new Vector3[2], null, 2.0f);
			line.color = colour;
		}
		line.points3.Clear (); 
		line.points3.Add (start3);
		line.points3.Add (end3);
		return line;
	}

	public VectorLine DrawListLine(VectorLine line, List<Vector3> points, Color colour){
		if (line == null) {
			line = new VectorLine("drawList", points, null, 2.0f, LineType.Continuous);
			line.color = colour;
			return line;
		} 
		line.points3.Clear();
		line.points3.AddRange (points);
		return line;
	}

	//For all rendered vessels
	private void UpdateDebugLines(Vessel vessel){
		if (vessel.waypoints.Length == 0) {
			ClearDebugLines(vessel);
			return;
		}

		int circleRes = 64;

		if (vessel.waypoints.Length != vessel.debugLines.Count - 1) {
			ClearDebugLines(vessel);
			if(vessel.waypoints.Length > 0){
				VectorLine trackLine = new VectorLine(string.Format("Track {0} - {1}", vessel.className, vessel.serialNumber), new List<Vector3>(), null, 2f, LineType.Continuous);
				
				trackLine.color = Color.green;
				//Needs 2 points to innitialize so 1 and 2 will be vessel pos.
				trackLine.points3.Add (WorldToGrid (vessel.position, 0));

				vessel.debugLines.Add (trackLine);
				foreach(NavWaypoint waypoint in vessel.waypoints){
					trackLine.points3.Add(WorldToGrid(waypoint.location, 0));
					VectorLine threshCircle = new VectorLine(string.Format("Threshold {0} - {1}", vessel.className, vessel.serialNumber), new Vector3[circleRes], null, 2f, LineType.Continuous);
					threshCircle.color = Color.green;
					threshCircle.MakeCircle(WorldToGrid(waypoint.location, 0), (float) waypoint.pointThreshold);
					vessel.debugLines.Add (threshCircle);
				}
			}
		}

		foreach(VectorLine line in vessel.debugLines){
			if(line.name.Contains("Track")){
				line.points3[0] = (WorldToGrid (vessel.position, 0));
			}
			line.Draw();
		}
	}

	public static void ClearDebugLines(Vessel vessel){
		if (vessel.debugLines == null) {
			vessel.debugLines = new List<VectorLine> ();
			return;
		}

		if (vessel.debugLines.Count == 0) {
			return;
		}
		VectorLine.Destroy(vessel.debugLines);
		vessel.debugLines.Clear ();
	}
	
	Color scanBlue = new Color32 (0, 150, 255, 150);
	Color scanRed = new Color32 (255, 0, 0, 150);
	public void UpdateScanCone(){
		if (!(targetSpaceObject is Vessel)) {
			scanModeOn = false;
			return;
		}


		int[] hitSerials = physMan.ConeScan (targetSpaceObject.serialNumber);
		Color coneColor = hitSerials.Length > 0 ? scanBlue: scanRed; 
		ScanConeMesh.staticRef.UpdateCone (((Vessel)targetSpaceObject).scanConeState, ((Vessel)targetSpaceObject).scanRange, coneColor);

		List<SpaceObject> overviewObjects = new List<SpaceObject> (); 
		foreach (int serial in hitSerials) {
			SpaceObject hitObj = physMan.GetObjectBySerial(serial, targetSpaceObject.serialNumber);
			if(hitObj == null){
				Debug.Log("updateScanCone got null hitVes.");
				continue;
			}
			overviewObjects.Add(physMan.GetObjectBySerial(serial, targetSpaceObject.serialNumber));
		}
		guiControl.SetOverviewObjects (overviewObjects);
	}

	public void ClearStage(){
		int[] spaceSerials = new int[visibleSpaceObjects.Count];
		visibleSpaceObjects.Keys.CopyTo(spaceSerials, 0);

		foreach(int serial in spaceSerials){
			RemoveObjectModel(visibleSpaceObjects[serial]);
		}
		visibleSpaceObjects.Clear ();
		targetSpaceObject = null;
	}

	//using a grid collider instead of this for now
	public Vector2 CastMouseToGrid(){
		Plane gridPlane = new Plane (Vector3.forward, Vector3.zero);
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		float rayDistance = 0;
		if (gridPlane.Raycast (ray, out rayDistance)) {
			Vector3 hitPoint = ray.GetPoint(rayDistance);
			return new Vector2(hitPoint.x, hitPoint.y);
		}
		Debug.LogError ("Ray didn't hit grid... how weird is that");
		return new Vector2(0,0);

	}

	private RaycastHit2D[] CastMouseToObjects(){
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		return Physics2D.GetRayIntersectionAll (camRay);
	}

	private NavWaypoint WaypointClick(){
		RaycastHit2D[] hits = CastMouseToObjects();
		Vector2 gridHit = Vector2.zero;
		foreach(RaycastHit2D hit in hits){
			string hitName = hit.transform.name;
			string[] splitName = hitName.Split('_');
			int hitSerial = -1;
			if(splitName.Length > 1){
				int.TryParse(splitName[1], out hitSerial);
			}
			if(hitName.Contains(PortSpawner._className)){
				Debug.Log(string.Format("Hit Port: hitName {0}, hitSerial {1}", hitName, hitSerial));
				return new NavWaypoint(hitSerial, ((Vessel)targetSpaceObject).waypointThreshold);
			}
			if(hitSerial >= 0){
				Debug.Log(string.Format("Hit: hitName {0}, name {1}, hitSerial {2}", hitName, splitName[0], hitSerial));
			}
			if(hitName.Equals("GridTarget")){
				gridHit = new Vector2(hit.point.x, hit.point.y);
				Debug.Log(string.Format("Hit grid at: {0}, {1}", gridHit.x, gridHit.y));
			}
		}

		return new NavWaypoint (GridToWorld (gridHit), ((Vessel)targetSpaceObject).waypointThreshold);
	}

	public void ResetCollideColours(){
		int[] serials = new int[visibleSpaceObjects.Count];
		visibleSpaceObjects.Keys.CopyTo (serials, 0);
		foreach (int serial in serials) {
			SetObjectColour(serial, Color.grey);
		}
	}

	public void SetObjectColour(int serial, Color newColor){
		if (!visibleSpaceObjects.ContainsKey (serial)) {
			Debug.Log ("Couldn't find serial " + serial + " to change colour");
			return;
		}
		//Will need more null checks if used for reals
		GameObject renderObj = visibleSpaceObjects [serial].GetRenderObject ();
		if (renderObj == null) {
			//Debug.Log ("set colour renderObj null");
			return;
		}
		Transform meshTran = renderObj.GetComponentInChildren<MeshFilter> ().transform;
		meshTran.GetComponent<Renderer>().material.color = newColor;
		//Debug.Log (string.Format ("Set {0} serial {1} to color: {2}", visibleSpaceObjects[serial].className, serial, newColor));

	}
	
	public Vector2 WorldToGrid(DoubleVect2 pos){
		return (pos - gridCenter).ToFlVector2();
	}

	public Vector3 WorldToGrid(DoubleVect2 pos, float zVal){
		Vector2 pos2 = WorldToGrid (pos);
		return new Vector3 (pos2.x, pos2.y, zVal);
	}
	public DoubleVect2 GridToWorld(Vector2 pos){
		return gridCenter + pos;
	}
	
}
