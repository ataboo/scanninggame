using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GUIOverview : MonoBehaviour {
	private List<SpaceObject> overviewObjects = new List<SpaceObject>();
	public static GUIOverview staticRef;
	public GameObject uiCanvas;
	public GameObject loadButtonPrefab;
	public GameObject loadContentPanel;

	public GameObject loadPane;
	public GameObject loadButtonBack;
	public GameObject savePane;
	public GameObject saveButtonBack;

	public GameObject saveFileInput;
	public GameObject saveFileOverWarning;

	public Vector2 spanButtonSize;
	private Vector2 menuScreenSize;

	public string saveName = "scansave";
	private string[] validFiles = new string[0];

	public static bool mainMenuOpen = false;
	//private Vector2 listSize;

	InputField saveField;
	

	void Start () {
		staticRef = this;
		this.saveField = saveFileInput.GetComponent<InputField> ();

	}
	/*




	void OnGUI(){
		Rect menuBackRect = new Rect(screenRes.x/2 - menuScreenSize.x/2, screenRes.y/2 - menuScreenSize.y/2, 
		                             menuScreenSize.x, menuScreenSize.y);
		if (_menuMode != MENUCLOSED) {

		}

		switch(_menuMode){
		case MENUCLOSED:
			int contactCount = 0;
			foreach (SpaceObject spaceObj in overviewObjects) {
				Rect posRect = new Rect(shipListCorner.x, shipListCorner.y + padding * contactCount, 
						labelSize.x, labelSize.y);
				GUI.Label(posRect, string.Format("Name: {0}, Class: {1}, Serial Number: {2}", spaceObj.name, spaceObj.className, spaceObj.serialNumber), overviewItemStyle);
				contactCount++;
			}
			break;

		case MAINMENU:
			GUI.Label(menuBackRect, "Main Menu", menuBackStyle);

			Rect spanButtonRect = new Rect ((screenRes.x - spanButtonSize.x)/2, menuBackRect.y + spanButtonSize.y + listSpacing, 
					spanButtonSize.x, spanButtonSize.y); 
			if (GUI.Button (spanButtonRect, "New Game...", buttonStyle)) {
				PhysicsManager.staticRef.NewGame();
				_menuMode = MENUCLOSED;
			}

			spanButtonRect.position += new Vector2 (0, spanButtonSize.y + listSpacing);
			if (GUI.Button (spanButtonRect, "Save Game...", buttonStyle)) {
				_menuMode = SAVEFILE;
				this.validFiles = SaveFileManager.GetValidSaveNames();
			}
			spanButtonRect.position += new Vector2 (0, spanButtonSize.y + listSpacing);
			if (GUI.Button (spanButtonRect, "Load Game...", buttonStyle)) {
				_menuMode = LOADFILE;
				this.validFiles = SaveFileManager.GetValidSaveNames();
			}
			spanButtonRect.position += new Vector2 (0, spanButtonSize.y + listSpacing);
			if (GUI.Button (spanButtonRect, "Quit", buttonStyle)) {
				PhysicsManager.staticRef.SaveFile("autosave.xml");
				Application.Quit();
			}
			break;

		case SAVEFILE:
			GUI.Label(menuBackRect, "Save Game to File:", menuBackStyle);

			Rect fileSaveRect = new Rect ((screenRes.x - spanButtonSize.x)/2, menuBackRect.position.y + 1.5f *(spanButtonSize.y + listSpacing), 
			                              spanButtonSize.x*0.8f, spanButtonSize.y); 
			saveName = GUI.TextField(fileSaveRect, saveName, 25, textFieldStyle);
			Rect saveButtonRect = new Rect(fileSaveRect.position.x + spanButtonSize.x*.8f, fileSaveRect.position.y,
			                               spanButtonSize.x * 0.2f, spanButtonSize.y);
			string fullName = saveName + ".xml";
			saveName = saveName.Replace("\n", "");

			if(GUI.Button(saveButtonRect, "Save", buttonStyle) || Input.GetKeyDown(KeyCode.Return) || fullName.Contains("\n")){
				fullName = fullName.Replace("\n", "");
				PhysicsManager.staticRef.SaveFile(fullName);
				_menuMode = MENUCLOSED;
			}

			foreach(string fileName in validFiles){
				if(fullName.Equals(fileName)){
					Rect overWarningRect = new Rect(fileSaveRect.position.x, saveButtonRect.position.y + spanButtonSize.y + listSpacing, 
					                                spanButtonSize.x, spanButtonSize.y);
					GUI.Label(overWarningRect, "WARNING! will overwrite existing save!", listItemStyle);
				}
			}

			break;

		case LOADFILE:
			GUI.Label(menuBackRect, "Load Game from File:", menuBackStyle);
			Rect fileLoadRect = new Rect ((screenRes.x - spanButtonSize.x)/2, menuBackRect.position.y + 1.5f * (spanButtonSize.y + listSpacing), 
			                              spanButtonSize.x, spanButtonSize.y); 
			if(validFiles.Length > 0){
				foreach(string validFileName in validFiles){

					if(GUI.Button(fileLoadRect, validFileName, listItemStyle)){
						PhysicsManager.staticRef.LoadFile(validFileName);
						_menuMode = MENUCLOSED;
					}
					fileLoadRect.position = new Vector2(fileLoadRect.position.x, fileLoadRect.position.y + spanButtonSize.y + listSpacing); 
				}
			} else{
				GUI.Label(fileLoadRect, "No Valid Save Files", buttonStyle);
			}
			break;
		}

	}
	*/

	//Called by UI togglebutton
	public void PopulateLoadScreen(){
		this.validFiles = SaveFileManager.GetValidSaveNames ();

		if (!loadPane.activeInHierarchy) {
			return;
		}
		for (int i=0; i< loadContentPanel.transform.childCount; i++){
			Transform.Destroy(loadContentPanel.transform.GetChild(i).gameObject);
		}


		foreach (string fileName in validFiles) {
			GameObject newLoadButton = Instantiate (loadButtonPrefab) as GameObject;
			UnityEngine.UI.Text uiText = newLoadButton.GetComponentInChildren<UnityEngine.UI.Text> ();
			uiText.text = fileName;
			Button loadButtonButton = newLoadButton.GetComponent<Button> ();
			string fileNameCaptured = fileName;  //Otherwise it just passes the last value from the loop
			loadButtonButton.onClick.AddListener(() => LoadFileFromButton(fileNameCaptured));
			newLoadButton.transform.SetParent (loadContentPanel.transform);
		}
	}
	

	public void HideLoadSave(){
		loadPane.SetActive (false);
		loadButtonBack.SetActive(false);
		savePane.SetActive (false);
		saveButtonBack.SetActive (false);

	}

	public void CheckOverwriteFile(){
		string newFileName = saveField.text;
		bool isOverwrite = false;
		foreach (string fileName in validFiles) {
			string fileStart = fileName.Split('.')[0];
			if(fileStart.Equals(newFileName, System.StringComparison.InvariantCultureIgnoreCase)){
				isOverwrite = true;
				//break;
			}
			Debug.Log(string.Format("Comparing {0} to {1} with outcome {2}", fileStart, newFileName, 
			                        fileStart.Equals(newFileName, System.StringComparison.InvariantCultureIgnoreCase)));
		}

		saveFileOverWarning.SetActive(isOverwrite);
	
	}

	public void OpenLoadScreen(){
		this.validFiles = SaveFileManager.GetValidSaveNames ();
		loadPane.SetActive (true);
		loadButtonBack.SetActive(true);
		savePane.SetActive (false);
		saveButtonBack.SetActive (false);
	}

	public void OpenSaveScreen(){
		this.validFiles = SaveFileManager.GetValidSaveNames ();
		savePane.SetActive (true);
		saveButtonBack.SetActive (true);
		loadPane.SetActive (false);
		loadButtonBack.SetActive (false);

		CheckOverwriteFile ();
	}

	public void SaveFile(){
		CheckOverwriteFile ();
		string fileName = saveField.text + ".xml";
		PhysicsManager.staticRef.SaveFile (fileName);

		ToggleMenu ();
	}

	public void LoadFileFromButton(string fileName){
		string[] fileSplit = fileName.Split ('.');
		if(!fileSplit[fileSplit.Length - 1].Equals("xml", System.StringComparison.InvariantCultureIgnoreCase)){
			fileName = fileName + ".xml";
		}
		PhysicsManager.staticRef.LoadFile(fileName);

		ToggleMenu ();
	}
	
	public void SetOverviewObjects(List<SpaceObject> objList){
		this.overviewObjects = objList;
	}

	//Called by stageMananger esc key
	public void ToggleMenu(){
		mainMenuOpen = !mainMenuOpen;
		uiCanvas.SetActive (mainMenuOpen);

		if (!mainMenuOpen) {
			HideLoadSave();
		}

		Vectrosity.VectorLine.canvas.gameObject.SetActive (!mainMenuOpen);
	}
}
