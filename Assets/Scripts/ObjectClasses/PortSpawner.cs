using UnityEngine;
using System.Collections;

[System.Serializable]
public class PortSpawner : SpaceObject {
	//Hard-Coded for now
	public int[] spawnProbs = new int[]{};
	public int[] spawnProbsCumulative = new int[]{};

	public DoubleVect2 inDockPos = DoubleVect2.zero;
	public DoubleVect2 outDockPos = DoubleVect2.zero;

	public FloatVect2 inDockOffset = FloatVect2.zero;
	public FloatVect2 outDockOffset = FloatVect2.zero;

	private static FloatVect2 spawnDelay = new FloatVect2(30, 60);
	public static string _className = "PortSpawner";

	//Spawning vars
	public float nextSpawnTime = 0f;
	//Lower, upper amount

	//For Serializer
	public PortSpawner(){
	}

	public PortSpawner(string name, DoubleVect2 position, float heading, int[] spawnProbs) : base(name, position){
		this.heading = heading;

		this.prefabName = "PortPrefab";
		this.className = _className;
		this.spawnProbs = spawnProbs;
		SetupDocks ();
	}

	public void SetupDocks(){
		this.inDockPos = this.outDockPos = position;
		if (inDockOffset != FloatVect2.zero || outDockOffset != FloatVect2.zero) {
			//inDockPos = inDockPos + MathUtils.RotateVect (inDockOffset, heading);
			//outDockPos = outDockPos + MathUtils.RotateVect (outDockOffset, heading);

			inDockPos += inDockOffset.Rotate(heading).ToDoubleVect2();
			outDockPos += outDockOffset.Rotate(heading).ToDoubleVect2();
		}
	}

	private void SetupForSpawning(){
		if (spawnProbs == null) {
			Debug.LogError(string.Format("Spawn probs null on portSpawner serial {0}", serialNumber));
			return;
		}

		this.spawnProbsCumulative = new int[spawnProbs.Length];  //Used for comparing ranges to random
		if (spawnProbs != null) {
			for(int i = 0; i<spawnProbs.Length; i++){
				spawnProbsCumulative[i] = 0;
				for(int j=i; j>=0; j--){
					spawnProbsCumulative[i] += spawnProbs[j];
				}
			}
		}

		nextSpawnTime = Time.time + Random.Range (0, spawnDelay.y - spawnDelay.x);
	}

	//Slow Polling possible
	public bool spawnUpdate(){
		if (spawnProbs.Length == 0) {
			Debug.LogError ("Port serial " + serialNumber + " shouldn't be asked to spawn");
			return false;
		}

		if (spawnProbsCumulative.Length != spawnProbs.Length) {
			SetupForSpawning();
		}

		if (PhysicsManager.staticRef.gameClock < nextSpawnTime) {
			return false;
		}

		nextSpawnTime = PhysicsManager.staticRef.gameClock + Random.Range (spawnDelay.x, spawnDelay.y);

		PhysicsManager physMan = PhysicsManager.staticRef;
		Vessel newBorn = (Vessel) physMan.SpawnVessel ((ConfigParser.SpaceObjectTypes)GetSpawnType (), outDockPos, heading);
		Debug.Log (string.Format ("PortSpawner {0} spawned a {1} with serial number: {2}", this.name, newBorn.className, newBorn.serialNumber));

		//Vessel will choose for itself later
		int neighbourDest = chooseDestinationPort(physMan.GetNeighbourPorts (serialNumber));
		PortSpawner neighbour = physMan.GetPort (neighbourDest);
		if (neighbour != null) {
			newBorn.SetDestination (new NavWaypoint (neighbour.serialNumber, newBorn.waypointThreshold)); 
			Debug.Log (string.Format ("{0} serial number: {1} added port: {2} (location {3}, {4}) as destination", newBorn.className, newBorn.serialNumber, neighbour.name,
					neighbour.position.x, neighbour.position.y));
		} else {
			Debug.LogError("neighbourLocation for serial: " + neighbourDest + " is null");
		}


		return true;
	}

	private int GetSpawnType(){
		//Range is (inclusive, exclusive)
		int rando = Random.Range (1, spawnProbsCumulative[spawnProbsCumulative.Length - 1] + 1);
		
		for (int i=0; i<spawnProbsCumulative.Length; i++) {
			if(rando <= spawnProbsCumulative[i]){
				return i;
			}
		}

		Debug.LogError ("PortSpawner getSpawnType did something weird.");
		return 0;
	}

	//Will use different weighting for map logic later
	private static int chooseDestinationPort(int[] neighbours){
		return neighbours[Random.Range (0, neighbours.Length)];
	}

	public void requestDock(Vessel vessel){
		PhysicsManager.staticRef.RemoveSpaceObject (vessel);
	}


}
