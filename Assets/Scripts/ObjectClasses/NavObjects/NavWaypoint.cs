using UnityEngine;
using System.Collections.Generic;
//Parent Class for waypoints

public class NavWaypoint{
	public DoubleVect2 location = DoubleVect2.zero;
	public double pointThreshold = 0d;
	public bool lastPoint = true;
	public int destoPortSerial = -1;

	//TODO: some kind of mode indicator

	//Only for Serializer
	public NavWaypoint(){

	}

	public NavWaypoint(DoubleVect2 position, float arrivalThreshold){
		this.location = position;
		this.pointThreshold = arrivalThreshold;
	}

	//Called if this is a docking waypoint
	public NavWaypoint(int destinationPortSerial, float arrivalThreshold){
		this.destoPortSerial = destinationPortSerial;
		PortSpawner destoPort = (PortSpawner) PhysicsManager.staticRef.GetPort (destinationPortSerial);
		this.location = destoPort.inDockPos;
		this.pointThreshold = arrivalThreshold;
	}

	public double RangeToPoint(DoubleVect2 position){
		return (position - location).Magnitude();
	}
	
	//Returns true if still navigating
	public bool UpdateSteering(Vessel parentVessel){
		
		//Assume full speed
		//parentVessel.desiredSpeed = parentVessel.speedLimits [1];
		parentVessel.desiredSpeed = parentVessel.speedLimits.y;
		double range = RangeToPoint (parentVessel.position);
		
		//Arrived at point
		if (range < pointThreshold) {
			parentVessel.desiredHeading = parentVessel.heading;
			parentVessel.desiredSpeed = 0;

			return false;
		}

		//Calculate desired heading towards target
		parentVessel.desiredHeading = MathUtils.clampEuler(MathUtils.VectorAngle (location - parentVessel.position));
		
		//If last point and bearing to point is offset hit the brakes.  If steer point, arrival range is 4 * threshold
		if (range < 10 * pointThreshold) {
			if(lastPoint){
				float steerOffset = Mathf.Abs (MathUtils.getSteerBearing (parentVessel.heading, parentVessel.desiredHeading));
				float steerCap = (float) (60 * range / 10 * pointThreshold + 5); //Max allowable steer offset at current range
				if(steerOffset > steerCap){
					Debug.Log ("breaking to turn...");
					parentVessel.desiredSpeed = 0;
					return true;
				}
			} else {
				return false;  //done mid steerpoint
			}
		}
		
		//Calculate lateral velocity relative to derired heading and steer to compensate
		
		double lateralVelocity = parentVessel.worldVelocity.DotProd(DoubleVect2.UnitVectFromAngle(parentVessel.desiredHeading + 90));
		//Debug.Log (string.Format("desiredHeading start: {0}, lateral Velocity: {1}", parentVessel.desiredHeading, lateralVelocity));
		parentVessel.desiredHeading  = MathUtils.clampEuler(parentVessel.desiredHeading - (float) lateralVelocity / parentVessel.speedLimits.y * 60f);
		//Debug.Log (string.Format ("desiredHeading after: {0}", parentVessel.desiredHeading)); 
		
		if (lastPoint) {
			//Calculate stop distance and set speed accordingly
			//d = Vi^2 / 2 * a
			float velMagSqr = (float) parentVessel.worldVelocity.MagSqr ();
			float stopDistance = velMagSqr / (parentVessel.enginePower * parentVessel.reverseThrust * 2);
			//Desired speed clamped to 0 because of case where ship is moving away from point but too close for stop from the point
			parentVessel.desiredSpeed = (float) MathUtils.ThresholdClamp(range - stopDistance, 0d, (double) parentVessel.speedLimits.y, 0.001d);  
		}
		
		return true;
	}

	//Checks course against static objects and paths around obstacles
	public static NavWaypoint[] ValidateCourse(Vessel vessel){
		return vessel.waypoints;

		/*
		if (vessel.waypoints.Length < 1) {
			Debug.LogError(string.Format("ValidateCourse: {0} serial {1} has no waypoints", vessel.className, vessel.serialNumber));
				return vessel.waypoints;
		}

		NavWaypoint[] waypoints = vessel.waypoints;
		List<SpaceObject> staticObjects = new List<SpaceObject> ();
		List<DoubleVect2[]> courseLegs = new List<DoubleVect2[]> ();

		foreach (int serial in vessel.detectableSerials) {
			SpaceObject spaceObject = PhysicsManager.staticRef.GetObjectBySerial (serial);
			if(spaceObject is Vessel)
				continue;
			staticObjects.Add(spaceObject);
		}
		for(int i=0; i<waypoints.Length; i++){
			if(i==0){
				courseLegs.Add(new DoubleVect2[]{vessel.position, waypoints[i].location});
				continue;
			}
			courseLegs.Add(new DoubleVect2[]{waypoints[i-1].location, waypoints[i].location});
		}
		*/
	}
}
