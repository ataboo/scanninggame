using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SpaceObject {
	public static float movementThreshold = PhysicsManager.movementThreshold;
	
	public DoubleVect2 position = new DoubleVect2(0,0);
	public string name = "Not Initialized";
	public string className = "Object";
	public int serialNumber = -1;
	public string prefabName = "Not Initialized";
	[System.NonSerialized]  //XML serializer ignores protected
	protected GameObject renderObject;
	public float heading = 0f;
	public float health = 100f;
	[System.Xml.Serialization.XmlIgnore][System.NonSerialized]
	public List<int> detectableSerials = new List<int>();
	public HardPoint[] hardPoints = new HardPoint[0];

	public CollisionTester.ColTypes colliderType = CollisionTester.ColTypes.Point;
	public FloatVect2[] hitBoxPoints = new FloatVect2[]{FloatVect2.zero};
	public float minColliderRadSqr = 0f;

	public SpaceObject(string name, DoubleVect2 position){
		Debug.Log (string.Format ("Setting position for {0} to {1}, {2}", name, position.x, position.y));
		this.name = name;
		this.position = new DoubleVect2(position.x, position.y);
		CollisionTester.SetMinColRadSqr (this);
	}

	//Used for serializing
	public SpaceObject(){
		CollisionTester.SetMinColRadSqr (this);
	}

	public virtual GameObject AddRenderObject(){
		if (renderObject == null) {
			this.renderObject = (GameObject)Transform.Instantiate (GetRenderPrefab (), StageManager.staticRef.WorldToGrid (position), Quaternion.Euler (0, 180, heading));
			this.renderObject.name = this.className + "_" + serialNumber;
		}
		return renderObject;
	}

	public GameObject GetRenderObject(){
		return renderObject;
	}

	public GameObject GetRenderPrefab(){
		GameObject renderPrefab = Resources.Load<GameObject> (prefabName);
		if (renderPrefab == null) {
			Debug.LogError("Couldn't find prefab" + prefabName);
		}
		return renderPrefab;
	}

	public void ClearRenderObject(){
		if (renderObject == null) {
			Debug.Log("Render object was null serial: " + serialNumber + ". Couldn't destroy it.");
			return;
		}
		
		Transform.Destroy (renderObject);
		this.renderObject = null;
	}

	public void ClearSerials(){
		if (detectableSerials == null) {
			detectableSerials = new List<int>();
			return;
		}
		detectableSerials.Clear ();
	}
}
