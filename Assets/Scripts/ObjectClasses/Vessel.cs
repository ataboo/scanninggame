using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vectrosity;
using System.Xml.Serialization;

[System.Serializable]
public class Vessel : SpaceObject {

	public static string _className = "Vessel";


	public DoubleVect2 lastPosition;
	public DoubleVect2 worldVelocity = DoubleVect2.zero;
	private FloatVect2 thrustAccel1 = FloatVect2.zero;

	public float desiredHeading = 0f;
	public float desiredRudder = 0f;
	public float desiredSpeed = 0f;
	public NavWaypoint[] waypoints = new NavWaypoint[0];  //Using array instead of list for serializing


	//TODO: move this to some script on game objects
	[XmlIgnore][System.NonSerialized]
	public List<VectorLine> debugLines = new List<VectorLine>();
	
	//TODO: Scanner as attached module
	public FloatVect2 scanConeState = new FloatVect2(0, 30); //(bearing, cone angleWidth)
	public FloatVect2 scanRanges = new FloatVect2(3000, 10000);
	public FloatVect2 scanSpreads = new FloatVect2(3, 90);
	public float scanSlewRate = 30f;
	public float scanRange = 0f;

	//Vessel Attributes set from config file
	public Thruster[] thrusters = new Thruster[0];
	public Weapon[] weapons = new Weapon[0];
	public FloatVect2 speedLimits = new FloatVect2(-50, 100);
	public float enginePower = 8; //Maximum engine acceleration.  Will change with damage, etc. also effects restore force
	public float keelFactor = 0.5f; //Restoring lateral force as factor of engine power
	public float reverseThrust = 0.5f;
	public float maxTurnRate = 30f;
	public float courseDelta = 0.1f; //Rate of player changing course
	public float throttleDelta = 1f;  //Rate of player changing throttle
	public float waypointThreshold = 20f; //Distance to waypoint before cleared

	public Vessel(string name, DoubleVect2 pos): base(name, pos){
		this.name = name;

	}

	//Only for serializer
	public Vessel() : base(){
		this.debugLines = new List<VectorLine> ();
		this.weapons = new Weapon[0];
	}

	public void SetDestination(DoubleVect2 pos){
		//Pathfinding logic
		SetDestination(new NavWaypoint(pos, waypointThreshold));
	}

	public void SetDestination(NavWaypoint desto){
		this.waypoints = new NavWaypoint[]{desto};

		this.waypoints = NavWaypoint.ValidateCourse (this);

		StageManager.ClearDebugLines (this);  //If updateDebugLines doesn't detect if waypoint count stays the same
	}

	public void TruncateWaypoint(DoubleVect2 pos){
		TruncateWaypoint (new NavWaypoint (pos, waypointThreshold));
	}

	public void TruncateWaypoint(NavWaypoint desto){
		if (waypoints.Length < 1) {
			SetDestination(desto);
			return;
		}
		NavWaypoint[] newWaypointsArr = new NavWaypoint [waypoints.Length + 1];
		waypoints.CopyTo (newWaypointsArr, 0);
		
		foreach (NavWaypoint waypoint in waypoints) {
			waypoint.lastPoint = false;
		}
		waypoints = newWaypointsArr;
		waypoints[waypoints.Length - 1] = desto;

		this.waypoints = NavWaypoint.ValidateCourse (this);
	}

	public void ClearWaypoints(){
		if (waypoints.Length == 0)
			return;
		this.desiredSpeed = 0;
		waypoints = new NavWaypoint[0];
	}

	public bool UpdateWaypoints(){
		if (waypoints.Length == 0) {
			return false;
		}

		//Find moving obstacles in path and react

		//Update steering returns false when reached waypoint removes next waypoint
		if (!waypoints [0].UpdateSteering (this)) {
			if(waypoints.Length == 1){
				Debug.Log("DestoPortSerial is: " + waypoints[0].destoPortSerial);
				if(waypoints[0].destoPortSerial >= 0){
					PortSpawner destoPort = PhysicsManager.staticRef.GetPort(waypoints[0].destoPortSerial);
					Debug.Log(string.Format("Vessel serial {0} request dock at serial {1}", serialNumber, destoPort.serialNumber));
					destoPort.requestDock(this);
				}
				waypoints = new NavWaypoint[0];
				return false;
			}
			NavWaypoint[] newWaypointsArr = new NavWaypoint[waypoints.Length - 1];
			for(int i=0; i<newWaypointsArr.Length; i++){
				newWaypointsArr[i] = waypoints[i+1];
			}
			waypoints = newWaypointsArr;
		}

		//Still navigating
		return true;
	}

	public void SteerTowards(float targetHeading){
		desiredRudder = Mathf.Clamp (MathUtils.getSteerBearing (heading, targetHeading), -maxTurnRate, maxTurnRate);
	}

	public override GameObject AddRenderObject(){
		if (renderObject != null) {
			Debug.Log ("Serial: " + serialNumber + " already rendered");
			return renderObject;
		}

		base.AddRenderObject ();
		if (hardPoints == null) {
			return renderObject;
		}

		foreach (Thruster thruster in thrusters) {
			thruster.AddRender(renderObject);
		}

		if (weapons != null) {

			foreach (Weapon weapon in weapons) {
				weapon.AddRender (renderObject);
			}
		}

		return renderObject;
	}

	public void UpdateRenders(){
		if (renderObject == null) {
			Debug.Log(string.Format("Tried to UpdateRender on {0} serial {1} but renderObject is null", className, serialNumber));
			return;
		}
		foreach (Thruster thruster in thrusters) {
			thruster.UpdateRender(thrustAccel1, desiredRudder/maxTurnRate);
		}

		if (weapons != null) {
			foreach(Weapon weapon in weapons) {
				weapon.UpdateRender ();
			}
		}
	}

	public void RotateScanCone(float rotation){
		scanConeState.x = MathUtils.clampEuler (scanConeState.x + rotation * scanSlewRate * Time.deltaTime);

	}

	public void SpreadScanCone(float widthChange){
		scanConeState.y = Mathf.Clamp(scanConeState.y + widthChange * scanSlewRate * Time.deltaTime, scanSpreads.x, scanSpreads.y);
	}

	public void UpdateScanRange(){
		float deltaRange = scanRanges.y - scanRanges.x;
		float deltaAngle = scanSpreads.y - scanSpreads.x;
		float angleFactor = (scanSpreads.y - scanConeState.y) / deltaAngle;
		this.scanRange = deltaRange * (angleFactor * angleFactor) + scanRanges.x;
		Debug.Log ("Set scan range to " + scanRange);
	}

	public void UpdateMovement(){
		if (waypoints.Length > 0) {
			UpdateWaypoints();
		}

		SteerTowards (desiredHeading);
		heading = MathUtils.clampEuler (heading + desiredRudder * Time.deltaTime);

		if (lastPosition == null)
			this.lastPosition = position; //Should force return anyways

		//get world velocity from position change

		DoubleVect2 deltaPos = position - lastPosition;

		//Debug.Log ("deltaTime: " + Time.deltaTime);
		if (MathUtils.Abs(deltaPos.x + deltaPos.y) < movementThreshold && desiredSpeed == 0f) {
			thrustAccel1 = FloatVect2.zero;
			this.lastPosition = position;
			//Debug.Log (string.Format("Vessel {0} serial {1} is slow enough to skip movement.", name, serialNumber));
			//thrustAccel1 = FloatVect2.zero;
			return;
		}

		this.worldVelocity = deltaPos / Time.deltaTime;

		//set lastPosition for comparison next call
		this.lastPosition = position;
		//Makes desired speed and direction vector
		DoubleVect2 desiredSpeedVect = new DoubleVect2(0, desiredSpeed).Rotate(desiredHeading);
		//Difference between desired and current velocity longitudinal to the vessel
		DoubleVect2 deltaVLong = (desiredSpeedVect - worldVelocity).Rotate (-heading);

		//Lateral and longitudinal acceleration oppose (the difference between desired velocity and current)
		//They are limited by engine power and rounded if close to 0
		float latAccel = Mathf.Clamp ((float) deltaVLong.x, -enginePower * keelFactor, enginePower * keelFactor);
		latAccel = (MathUtils.InThreshold ((float) deltaVLong.x, 0, movementThreshold / 10)) ? 0f: latAccel;
		float longAccel = MathUtils.ThresholdClamp((float) deltaVLong.y, -enginePower * reverseThrust, enginePower, movementThreshold/100);
		longAccel = (MathUtils.InThreshold ((float) deltaVLong.y, 0, movementThreshold / 10)) ? 0f: longAccel; 
		DoubleVect2 thrustAccel = new DoubleVect2 (latAccel, longAccel);
		thrustAccel = MathUtils.ThresholdZeroVector (thrustAccel, movementThreshold / 10);

		//Rotate accel vector by heading to get world acceleration vector
		DoubleVect2 worldAccel = MathUtils.RotateVect (thrustAccel, heading);

		this.thrustAccel1 = (thrustAccel / enginePower).ToFloatVect2(); //-1<thrustaccel<1 for thruster rendering
		//Add world acceleration to world velocity
		this.worldVelocity = worldVelocity + worldAccel * Time.deltaTime; //Also referenced by waypoints

		//update vessel from world velocity
		position = position + worldVelocity * Time.deltaTime;

		//TODO: move this to stageManager/prefab scripts
		/*
		if (name.Equals("HMS Playerface")) {
			StageManager.staticRef.UpdateDebugLines(this, desiredSpeedVect, deltaVLong, thrustAccel.divide(updateDelta));
		}
		*/
	}

	public void FireWeapons(){
		foreach (Weapon weapon in weapons) {
			weapon.FireWeapon(this);
		}
	}


}
