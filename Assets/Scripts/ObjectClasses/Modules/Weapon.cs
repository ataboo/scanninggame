using System;
using UnityEngine;
using Vectrosity;

[System.Serializable]
public class Weapon : ShipModule{
	public enum WeaponPrefabs {MedRail, MedLaser, Empty};
	public static readonly string[] weaponPrefabNames = {"MedRailTurret", "MedLaserTurret", "Empty"};

	public enum WeaponCategory {Launcher, Ballistic, Energy};

	public FloatVect2 weaponArc = FloatVect2.zero;
	public float weaponHeading = 0;
	public float lastFired = 0;

	public WeaponCategory weaponCategory = WeaponCategory.Ballistic;
	public float maxRange = 1800;
	public float falloffRange = 1200;
	public float rotationSpeed = 10;
	public float reloadTime = 10;
	public FloatVect2 damageBrackets= new FloatVect2(20, 30);
		

	//for Serializer
	public Weapon(){
	}

	public static Weapon MakeWeapon(HardPoint hardPoint){
		WeaponPrefabs weaponType = hardPoint.weaponType;
		Weapon weapon = ConfigParser.MakeWeaponFromConfig (weaponType);
		weapon.hpPosition = hardPoint.localPosition;
		weapon.hpHeight = hardPoint.localHeight;
		weapon.hpRotation = hardPoint.rotation;
		weapon.weaponArc = hardPoint.turretArc;

		return weapon;
	}

	public void UpdateRender(FloatVect2 thrustVector, float turnRate){

	}

	public void TurnTowards(DoubleVect2 targetPosition){

	}

	public void FireWeapon(SpaceObject parentObject){
		if (lastFired + reloadTime > Time.timeSinceLevelLoad) {
			Debug.Log("Still Reloading");
			return;
		}
		lastFired = Time.timeSinceLevelLoad;
		StageManager stageMan = StageManager.staticRef;
		Vector3 hpOffsetGrid = stageMan.WorldToGrid(parentObject.position, hpHeight);

		DoubleVect2 beamEndPos = new DoubleVect2(0, maxRange).Rotate (MathUtils.clampEuler(weaponHeading + parentObject.heading));
		beamEndPos += parentObject.position;
		Vector3 endPoint = stageMan.WorldToGrid (beamEndPos, 0) + hpOffsetGrid;
		
		Vector3 startPoint = hpOffsetGrid;
		VectorLine.SetLine (Color.red, 1.0f, startPoint, endPoint);
	}
}


