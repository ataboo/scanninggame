﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class HardPointPrefab : MonoBehaviour {
	public HardPoint.PointType pointType = HardPoint.PointType.Thruster;
	//TODO: FloatVect3
	public FloatVect2 localPosition;
	public float height;
	public float localRotation;
	public int hpSerial;
	
	public Thruster.ThrustQuad thrustQuad = Thruster.ThrustQuad.Bow;
	public Thruster.ThrustSize thrustSize = Thruster.ThrustSize.Normal;
	public Thruster.ThrustTurn thrustTurn = Thruster.ThrustTurn.None;

	public HardPoint.DockType dockType = 0;
	
	public FloatVect2 turretArc = FloatVect2.zero;
	public Weapon.WeaponPrefabs defaultWeapon;
	public string defaultWeaponName = string.Empty;
	
	public void GetHardPoint(int hpSerial){
		this.localPosition = new FloatVect2 (transform.localPosition.x, transform.localPosition.y);
		this.height = transform.localPosition.z;
		this.localRotation = transform.localEulerAngles.z;
		this.hpSerial = hpSerial;
		//Verify inspectorSettings;
	}
}

[CustomEditor(typeof(HardPointPrefab))]
public class HardPointEditor : Editor {
	
	public override void OnInspectorGUI(){
		HardPointPrefab hpPrefab = (HardPointPrefab)target;
		hpPrefab.pointType = (HardPoint.PointType)EditorGUILayout.EnumPopup ("Hardpoint type", hpPrefab.pointType);
		//EditorGUILayout.LabelField ("Level", hardPointPrefab.Level.ToString());
		if (hpPrefab.pointType == HardPoint.PointType.Thruster) {
			hpPrefab.thrustQuad = (Thruster.ThrustQuad)EditorGUILayout.EnumPopup("Quadrant", hpPrefab.thrustQuad);
			hpPrefab.thrustTurn = (Thruster.ThrustTurn)EditorGUILayout.EnumPopup("Rotation", hpPrefab.thrustTurn);
			hpPrefab.thrustSize = (Thruster.ThrustSize)EditorGUILayout.EnumPopup("Size", hpPrefab.thrustSize);
		}

		if (hpPrefab.pointType == HardPoint.PointType.Weapon) {
			Vector2 hpArcVect = hpPrefab.turretArc.ToFlVector2();
			hpArcVect = EditorGUILayout.Vector2Field("Weapon Arc", hpArcVect);
			hpPrefab.turretArc = new FloatVect2(hpArcVect);
			hpPrefab.defaultWeapon = (Weapon.WeaponPrefabs)EditorGUILayout.EnumPopup("Default Weapon", hpPrefab.defaultWeapon);
			hpPrefab.defaultWeaponName = Weapon.weaponPrefabNames[(int)hpPrefab.defaultWeapon];
			EditorGUILayout.LabelField("Weapon Prefab Name: " + hpPrefab.defaultWeaponName);

		}
		if (hpPrefab.pointType == HardPoint.PointType.Dock) {
			hpPrefab.dockType = (HardPoint.DockType)EditorGUILayout.EnumMaskField("Dock Type", hpPrefab.dockType);
		}
		
	}
}
