using UnityEngine;
using System.Collections;

[System.Serializable]
public class ShipModule{

	//public GameObject prefabObject;
	[System.NonSerialized]
	protected GameObject renderObject;
	public string prefabName = string.Empty;
	public float health = 100;
	public float hpRotation = 0;
	public FloatVect2 hpPosition = FloatVect2.zero;
	public float hpHeight = 0f;

	public ShipModule(){}

	public virtual void UpdateRender(){}

	public void AddRender(GameObject parentObject){
		if (prefabName == string.Empty) {
			Debug.LogError(string.Format("ShipModule: {0} tried to AddRender with no prefabName set.", parentObject.name));
			return;
		}

		if (renderObject != null) {
			Debug.Log(string.Format("ShipModule: {0} tried to add module when one was already rendering", parentObject.name));
			return;
		}

		if(prefabName.Equals("Empty")){
			return;
		}
		
		GameObject prefabObject = (GameObject) Resources.Load(prefabName);
		this.renderObject = (GameObject) Transform.Instantiate(prefabObject, parentObject.transform.position, parentObject.transform.rotation);
		renderObject.transform.SetParent(parentObject.transform);
		renderObject.transform.localPosition = new Vector3(hpPosition.x, hpPosition.y, hpHeight);
		renderObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, hpRotation));
		
		return;
	}
	
	//Not really needed for now. Just destroying parent.
	public void ClearRender(){
		Transform.Destroy (renderObject);
		this.renderObject = null;
	}
}