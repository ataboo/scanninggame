﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class WeaponConfigGen : MonoBehaviour {
	public Weapon.WeaponCategory weaponCat;
	public string prefabName;
	//Turret Values
	public float maxRange = 1800;
	public float falloffRange = 1200;
	public float rotationSpeed = 10;
	public float reloadTime = 10;
	public FloatVect2 damageBrackets= new FloatVect2(20, 30);

	void Start(){
		this.prefabName = transform.name;
		GenConfig ();
	}

	private void GenConfig(){
		Weapon weapon = new Weapon ();
		weapon.weaponCategory = weaponCat;
		weapon.prefabName = prefabName;
		weapon.maxRange = maxRange;
		weapon.falloffRange = falloffRange;
		weapon.rotationSpeed = rotationSpeed;
		weapon.reloadTime = reloadTime;
		weapon.damageBrackets = damageBrackets;

		FileSerializer.SaveClassToXML (prefabName, weapon);
		FileSerializer.SaveClassToBin (prefabName, weapon);
	}
}

[CustomEditor(typeof(WeaponConfigGen))]
public class WeaponEditor : Editor {
	
	public override void OnInspectorGUI(){
		WeaponConfigGen wepPrefab = (WeaponConfigGen)target;
		wepPrefab.weaponCat = (Weapon.WeaponCategory)EditorGUILayout.EnumPopup ("Weapon Category", wepPrefab.weaponCat);

		switch (wepPrefab.weaponCat) {
		case Weapon.WeaponCategory.Ballistic:
		case Weapon.WeaponCategory.Energy:
			wepPrefab.maxRange = EditorGUILayout.FloatField("Max range", wepPrefab.maxRange);
			wepPrefab.falloffRange = EditorGUILayout.FloatField("Damage falloff", wepPrefab.falloffRange);
			wepPrefab.rotationSpeed = EditorGUILayout.FloatField("Rotation speed", wepPrefab.rotationSpeed);
			wepPrefab.reloadTime = EditorGUILayout.FloatField("Reload time", wepPrefab.reloadTime);
			Vector2 damageBracketV2 = wepPrefab.damageBrackets.ToFlVector2();
			damageBracketV2 = EditorGUILayout.Vector2Field("Damage brackets", damageBracketV2);
			wepPrefab.damageBrackets = new FloatVect2(damageBracketV2);
			break;
		}
	}
}
