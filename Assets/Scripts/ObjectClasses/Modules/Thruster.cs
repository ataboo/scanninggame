using System;
using UnityEngine;

[System.Serializable]
public class Thruster : ShipModule{
	[System.NonSerialized]
	private ParticleSystem thrustParticles;
	public static float[,] emmisionSpeeds = {{1, 40},{1, 50}};  //Small, Large
	public static float[,] particleSizes = {{1.2f, 5}, {2f, 9}};
	public float[] partSizes;
	public float[] emitSpeeds;
	private static readonly string[] prefabThrusterNames = {"ThrusterAPrefab","ThrusterBPrefab"};

	public enum ThrustQuad {Bow, Stern, Star, Port};
	public enum ThrustTurn {None, Clock, Anti};
	public enum ThrustSize {Normal, Big};

	public ThrustQuad quad = ThrustQuad.Stern;
	public ThrustTurn turn = ThrustTurn.None;
	public ThrustSize size = ThrustSize.Normal;

	//for Serializer
	public Thruster(){
		this.prefabName = prefabThrusterNames[(int) size];
	}
	
	public Thruster (HardPoint hardPoint){
		this.quad = hardPoint.thrustQuadrant;
		this.turn = hardPoint.thrustTurn;
		this.size = hardPoint.thrustSize;

		this.prefabName = prefabThrusterNames[(int)size];

		if (particleSizes.GetLength (1) != 2 || emmisionSpeeds.GetLength (1) != 2) {
			Debug.LogError("Thruster: emmisionSpeeds or particleSizes malformed");
			return;
		}
		this.partSizes = new float[2];
		this.emitSpeeds = new float[2];
		for(int i=0; i<partSizes.Length; i++){
			partSizes[i] = particleSizes[(int)size, i];
			emitSpeeds[i] = emmisionSpeeds[(int)size, i];
		}

		this.hpPosition = hardPoint.localPosition;
		this.hpRotation = hardPoint.rotation;
	}

	/*
	public override void AddRender(GameObject parentObject){
		if (renderObject != null) {
			Debug.Log("Tried to add thruster when one was already rendering");
			return;
		}

		GameObject prefabObject = (GameObject) Resources.Load(prefabName);
		this.renderObject = (GameObject) Transform.Instantiate(prefabObject, parentObject.transform.position, parentObject.transform.rotation);
		renderObject.transform.SetParent(parentObject.transform);
		renderObject.transform.localPosition = new Vector3(hpPosition.x, hpPosition.y, 0);
		renderObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, hpRotation));

		return;
	}

	//Not really needed for now. Just destroying parent.
	public override void ClearRender(){
		Transform.Destroy (renderObject);
		this.renderObject = null;
	}
	*/
	
	public void UpdateRender(FloatVect2 thrustVector, float turnRate){
		if (turn != ThrustTurn.None && Mathf.Abs(turnRate) > 0.1f) {
			SetParticles((turn == ThrustTurn.Clock) ? turnRate/2: -turnRate/2);
			return;
		}

		float thrustValue = 0;
		switch (quad) {
		case ThrustQuad.Bow:
			thrustValue = -thrustVector.y;
			break;
		case ThrustQuad.Stern:
			thrustValue = thrustVector.y;
			break;
		case ThrustQuad.Star:
			thrustValue = -thrustVector.x;
			break;
		case ThrustQuad.Port:
			thrustValue = thrustVector.x;
			break;
		}
		SetParticles(thrustValue);
	}
	
	private void SetParticles(float value){
		if (thrustParticles == null) {
			if(renderObject == null){
				Debug.Log ("Tried to set particles on a thruster without render object");
				return;
			}
			this.thrustParticles = renderObject.GetComponentInChildren<ParticleSystem>();
			if(thrustParticles == null){
				Debug.Log("Couldn't find particle system in thruster");
				return;
			}
		}


		thrustParticles.enableEmission = value > 0;
		thrustParticles.startSize = MathUtils.WeightedAverage(partSizes, value);
		thrustParticles.startSpeed = MathUtils.WeightedAverage(emitSpeeds, value);
	}
}

