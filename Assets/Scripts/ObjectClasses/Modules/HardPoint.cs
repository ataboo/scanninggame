﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;


[XmlRoot ("HardPoint")] [System.Serializable]
public class HardPoint {
	public enum PointType{Thruster, Weapon, Dock, Empty};

	[System.Flags]  // (DockType.InDock | DockType.OutDock) for both
	public enum DockType{InDock = 1, OutDock = 2};
	public DockType dockType = 0;
	public PointType pointType = PointType.Empty;
	public FloatVect2 localPosition;
	public float localHeight;
	public float rotation;
	public int hpSerial;
	public Thruster.ThrustQuad thrustQuadrant;
	public Thruster.ThrustTurn thrustTurn;
	public Thruster.ThrustSize thrustSize;

	public FloatVect2 turretArc = FloatVect2.zero;
	public Weapon.WeaponPrefabs weaponType = Weapon.WeaponPrefabs.Empty;
	
	public static HardPoint MakeHardPoint(Transform child, int hardPointCount, string prefabName){
		HardPointPrefab hpPrefab = child.GetComponent<HardPointPrefab>();
		hpPrefab.GetHardPoint (hardPointCount);
		if(hpPrefab == null){
			Debug.Log(string.Format("HardPoint: Hardpoint named {0} on {1} prefab doesn't have an attached HardPointPrefab script", child.name, prefabName));
			return new HardPoint();
		}
		
		if(hpPrefab.pointType == PointType.Thruster){ //Thruster Point
			return MakeThrust(hpPrefab);
		}

		if (hpPrefab.pointType == PointType.Weapon) {
			return MakeWeapon(hpPrefab);	
		}


		if (hpPrefab.pointType == PointType.Dock) {
			return MakeDock(hpPrefab);
		}

		Debug.Log(string.Format("HardPoint: Hardpoint named {0} on {1} prefab fell through MakeHardPoint", child.name, prefabName));
		return new HardPoint();
	}
			           

	public HardPoint(){
		this.pointType = PointType.Empty;
		this.localPosition = FloatVect2.zero;
		this.rotation = 0f;
		this.hpSerial = -1;
	}

	public HardPoint(HardPointPrefab hpPrefab){
		this.pointType = hpPrefab.pointType;
		this.hpSerial = hpPrefab.hpSerial;
		this.localPosition = hpPrefab.localPosition;
		this.localHeight = hpPrefab.height;
		this.rotation = hpPrefab.localRotation;
	}

	public void AddDefaultModule(SpaceObject spaceObj){
		if (!(spaceObj is Vessel)) {
			Debug.Log ("Can only add modules to vessels so far");
			return;
		}
		Vessel parVessel = (Vessel)spaceObj;

		switch(pointType){
		case PointType.Thruster:
			int nextOpenSpot = findNextArrSpot(parVessel.thrusters);
			if(nextOpenSpot<0){
				Debug.LogError (string.Format("No open spot for a thruster in {0} named {1} serial {2}", parVessel.className, parVessel.name, parVessel.serialNumber));
				return;
			}
			parVessel.thrusters[nextOpenSpot] = new Thruster(this);
			break;
		case PointType.Weapon:
			int nextWeaponSpot = findNextArrSpot(parVessel.weapons);
			if(nextWeaponSpot<0){
				Debug.LogError (string.Format("No open spot for a thruster in {0} named {1} serial {2}", parVessel.className, parVessel.name, parVessel.serialNumber));
				return;
			}
			Weapon newWeapon = Weapon.MakeWeapon(this);

			parVessel.weapons[nextWeaponSpot] = newWeapon;
			break;
		case PointType.Dock:
			Debug.LogError(string.Format("HardPoint: {0} serial {1} tried to generate module on dock.", spaceObj.className, spaceObj.serialNumber));
			return;
		case PointType.Empty:
			Debug.LogError(string.Format("HardPoint: {0} serial {1} tried to generate module on empty Hardpoint.", spaceObj.className, spaceObj.serialNumber));
			return;
		}
		


	}
	
	private int findNextArrSpot(ShipModule[] modArr){
		for (int i=0; i<modArr.Length; i++) {
			if(modArr[i] == null) return i;
		}
		return -1;
	}

	//child.localPosition, child.localEulerAngles.z, hardPointCount
	private static HardPoint MakeDock(HardPointPrefab hpPrefab){
		HardPoint hardPoint = new HardPoint (hpPrefab);
		hardPoint.dockType = hpPrefab.dockType;
		return hardPoint;
	}
	
	private static HardPoint MakeThrust(HardPointPrefab hpPrefab){
		HardPoint hardPoint = new HardPoint (hpPrefab);
		hardPoint.thrustQuadrant = hpPrefab.thrustQuad;
		hardPoint.thrustSize = hpPrefab.thrustSize;
		hardPoint.thrustTurn = hpPrefab.thrustTurn;
		return hardPoint;
	}

	private static HardPoint MakeWeapon(HardPointPrefab hpPrefab){
		HardPoint weaponPoint = new HardPoint(hpPrefab);
		weaponPoint.turretArc = hpPrefab.turretArc;
		weaponPoint.weaponType = hpPrefab.defaultWeapon;
		return weaponPoint;
	}
}
