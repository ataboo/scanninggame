﻿using UnityEngine;
using System;

public static class MathUtils {

	//Interpolate and return target if withing threshold
	public static float ThresholdLerp(float val, float target, float rate, float threshold){
		if (Mathf.Abs(val - target) <= threshold) {
			return target;
		}
		return Mathf.Lerp (val, target, rate);
	}
	
	//Clamp value but return extremes if within threshold
	public static float ThresholdClamp(float value, float floor, float ceiling, float threshold){
		if (Mathf.Abs (value - floor) < threshold)
			return floor;
		if (Mathf.Abs (value - ceiling) < threshold)
			return ceiling;
		return Mathf.Clamp(value, floor, ceiling);
	}

	public static double ThresholdClamp(double value, double floor, double ceiling, double threshold){
		if (Abs (value - floor) < threshold)
			return floor;
		if (Abs (value - ceiling) < threshold)
			return ceiling;
		return (value < floor) ? floor : (value > ceiling) ? ceiling : value;
	}

	public static double Abs (double value){
		return (value < 0) ? -value : value;
	}

	public static float Abs (float value){
		return (value < 0) ? -value : value;
	}
	
	private static string PrintVector(Vector2 v){
		return " x: " + v.x + " y: " + v.y;
	}
	
	public static Vector2 thresholdVector(Vector2 vect, Vector2 target, float threshold){
		return new Vector2 (thresholdFloat (vect.x, target.x, threshold), thresholdFloat (vect.y, target.y, threshold));
	}

	public static DoubleVect2 ThresholdZeroVector(DoubleVect2 vect, float threshold){
		vect.x = InThreshold ((float)vect.x, 0, threshold) ? 0d: vect.x;
		vect.y = InThreshold ((float)vect.y, 0, threshold) ? 0d : vect.y;
		return vect;
	}
	
	public static float thresholdFloat(float val, float target, float threshold){
		return (Mathf.Abs (val - target) < threshold) ? target : val; 
	}

	public static double sqrt(double val){
		return Math.Sqrt (val);
	}

	public static DoubleVect2 RotateVect(DoubleVect2 vect, Quaternion quat){
		Vector2 rotated = quat * vect.ToFlVector2();
		return new DoubleVect2(rotated);
	}

	public static float clampEuler(float angle){
		if (angle >= 0 && angle < 360)
			return angle;
		angle =  angle - (int) angle + ((int)angle % 360);
		if (angle < 0)
			angle += 360;

		return angle;
	}

	//Rotates vector clockwise;
	public static DoubleVect2 RotateVect(DoubleVect2 vect, double thetaClock){
		//Base formula rotates counter clockwise
		thetaClock = -degToRad (thetaClock);
		double cosTheta = Math.Cos (thetaClock);
		double sinTheta = Math.Sin (thetaClock);
		return new DoubleVect2(vect.x * cosTheta - vect.y * sinTheta, vect.x * sinTheta + vect.y * cosTheta);
	}

	public static Vector2 RotateVect(Vector2 vect, double thetaClock){
		thetaClock = -degToRad (thetaClock);
		float cosTheta = (float) Math.Cos (thetaClock);
		float sinTheta = (float) Math.Sin (thetaClock);
		return new Vector2 (vect.x * cosTheta - vect.y * sinTheta, vect.x * sinTheta + vect.y * cosTheta);
	}

	public static double degToRad(double deg){
		return deg * Math.PI / 180d;
	}

	public static double radToDeg(double rad){
		return rad * 180d / Math.PI;
	}

	//returns negative or positive bearing (left, right) difference under 180
	public static float getSteerBearing(float heading, float target){
		float bearing = clampEuler (target - heading);
		float course = bearing - ((int)bearing / 180) * 360;
		//Debug.Log (String.Format ("heading: {0}, target: {1}, bearing: {2}, course: {3}", heading, target, bearing, course));
		//Debug.Log (String.Format ("bearing/180: {0}", (int)bearing/180));

		return course;
	}

	public static float VectorAngle(DoubleVect2 vect){
		return (float) radToDeg(Math.Atan2 (vect.x, vect.y));
	}

	public static float WeightedAverage(float[] range, float weight){
		if (range.Length < 2)
			return 0f;
		Mathf.Clamp01 (weight);

		return (range[1] - range[0]) * weight + range[0];
	}

	public static bool InThreshold(float val, float target, float threshold){
		return Mathf.Abs (target - val) < threshold;
	}

	public static bool lineIntersection(DoubleVect2 start1, DoubleVect2 end1, DoubleVect2 start2, DoubleVect2 end2){
		//credit to Gareth Rees for nice StackOverflow example: 
		//http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
		//u = (q-p) x r / (r x s)
		//t = (q-p) x s / (r x s)
		//q-p = start1 - start2
		//r = end1 - start1
		//s = end2 - start2
		DoubleVect2 r = end1 - start1;
		DoubleVect2 s = end2 - start2;
		double rCrossS = r.CrossProd (s);
		if (rCrossS == 0) {
			Debug.Log ("Lines are Collinear");
			return false;
		}
		DoubleVect2 qMinusP = start1 - start2;
		double u = qMinusP.CrossProd (r) / rCrossS;
		double t = qMinusP.CrossProd (s) / rCrossS;

		if (Between01 (u) && Between01 (t)) {
			return true;
		}

		//intersect = p + tr = q + us if need point later

		return false;
	}

	public static bool Between01(double value){
		return (!(value > 1 || value < 0));
	}

	//Returns (lateral, longitudinal) space between target on heading axis (track)
	public static DoubleVect2 SpreadByHeading(DoubleVect2 startPosition, DoubleVect2 target, float heading){
		DoubleVect2 relativePos = target - startPosition;
		float headingRad = (float) degToRad (heading);

		DoubleVect2 headingUnit = new DoubleVect2 (Mathf.Sin (headingRad), Mathf.Cos (headingRad));
		return new DoubleVect2(relativePos.DotProd(headingUnit), relativePos.DotProd(headingUnit.PerpVector()));
	}

}
