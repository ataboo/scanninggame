using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public static class SaveFileManager {

	public static readonly string outputPath = Application.persistentDataPath + "/SaveFiles/";
	public static readonly string outputSuffix = ".xml";

	public static void SaveActiveObjects(string fileName, float gameTime, SpaceObject[] activeObjects){
		BuildSaveDirectory ();

		List<Vessel> activeVessels = new List<Vessel>();
		List<PortSpawner> activePorts = new List<PortSpawner>();

		foreach (SpaceObject spaceObj in activeObjects) {
			if(spaceObj is Vessel){
				activeVessels.Add((Vessel)spaceObj);
				continue;
			}
			if(spaceObj is PortSpawner){
				activePorts.Add ((PortSpawner) spaceObj);
				continue;
			}
		}
		SaveHolder saveHolder = new SaveHolder (gameTime, activeVessels.ToArray (), activePorts.ToArray ());

		XmlSerializer serializer = new XmlSerializer (typeof(SaveHolder));
		

		FileStream stream = new FileStream (outputPath + fileName, FileMode.Create);
		serializer.Serialize (stream, saveHolder);
		stream.Close ();

	}

	public static void BuildSaveDirectory(){
		if (!System.IO.Directory.Exists (outputPath)) {
			System.IO.Directory.CreateDirectory (outputPath);
		}
	}

	public static SpaceObject[] LoadActiveObjects(string fileName){
		BuildSaveDirectory ();

		XmlSerializer serializer = new XmlSerializer (typeof(SaveHolder));
		FileStream stream = new FileStream (outputPath + fileName, FileMode.Open);
		SaveHolder saveHolder = (SaveHolder) serializer.Deserialize (stream);
		stream.Close ();

		SpaceObject[] returnArray = new SpaceObject[saveHolder.savedVessels.Length + saveHolder.savedPorts.Length];
		saveHolder.savedVessels.CopyTo (returnArray, 0);
		saveHolder.savedPorts.CopyTo (returnArray, saveHolder.savedVessels.Length);
		PhysicsManager.staticRef.gameClock = saveHolder.saveTime;
		return returnArray;
	}

	public static string[] GetValidSaveNames(){
		BuildSaveDirectory ();

		string[] checkFiles = Directory.GetFiles (outputPath);
		List<string> validFiles = new List<string> ();

		XmlSerializer serializer = new XmlSerializer (typeof(SaveHolder));
		foreach (string fileName in checkFiles) {
			FileStream stream = new FileStream (fileName, FileMode.Open);
			XmlReader xmlReader = XmlReader.Create (stream);
			if (serializer.CanDeserialize (xmlReader)) {
				string[] splitFile = fileName.Split('/');
				validFiles.Add(splitFile[splitFile.Length-1]);
			}
			xmlReader.Close();
			stream.Close ();
		}

		return validFiles.ToArray ();
	}

}

public class SaveHolder{
	public Vessel[] savedVessels;
	public PortSpawner[] savedPorts;
	public float saveTime = 0;

	public SaveHolder(){
		savedVessels = new Vessel[0];
		savedPorts = new PortSpawner[0];
	}

	public SaveHolder(float saveTime, Vessel[] vessels, PortSpawner[] ports){
		this.savedVessels = vessels;
		this.savedPorts = ports;
		this.saveTime = saveTime;
	}
}

