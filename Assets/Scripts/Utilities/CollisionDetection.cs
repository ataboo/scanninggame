﻿using UnityEngine;
using System.Collections;


public class CollisionDetection : MonoBehaviour {


	public GameObject indicatorBox;
	public GameObject colliderB;

	private MeshRenderer indicatorRender;
	private PolygonCollider2D polyCol;
	private PolygonCollider2D polyColB;

	// Use this for initialization
	void Start () {
		indicatorRender = transform.GetComponentInChildren<MeshRenderer> ();
		indicatorRender.material.color = Color.red;
		this.polyCol = transform.GetComponent<PolygonCollider2D> ();
		this.polyColB = colliderB.transform.GetComponent<PolygonCollider2D> ();
	}

	void Update(){
		if(Input.GetMouseButtonDown(0)){
			indicatorRender.material.color = RunSATTest () ? Color.green: Color.red;
		}

	}
	
	// Update is called once per frame
	private bool RunSATTest () {
		Vector2[] polyPointsA = polyCol.points;
		Vector2[] polyPointsB = polyColB.points;


		Vector2 positionA = new Vector2 (transform.position.x, transform.position.y);
		Vector2 positionB = new Vector2 (colliderB.transform.position.x, colliderB.transform.position.y);
		for (int i=0; i<polyPointsA.Length; i++) {
			polyPointsA[i] = polyPointsA[i] + positionA;
		}
		for (int i=0; i<polyPointsB.Length; i++) {
			polyPointsB[i] = polyPointsB[i] + positionB;
		}

		for (int i=0; i<polyPointsA.Length; i++) {
			Vector2 p0 = polyPointsA[i];
			Vector2 p1 = (i < polyPointsA.Length -1) ? polyPointsA[i+1]: polyPointsA[0];
			Debug.Log (string.Format ("Point {0} is {1}", i, polyPointsA[i].x));

			//Get Normal axis from edge
			Vector2 normPerp = GetNormalPerp(p1-p0);
			Vector2 minMax = GetMinMax(polyPointsA, normPerp);
			Vector2 minMaxB = GetMinMax(polyPointsB, normPerp);
			Debug.Log(string.Format("minMaxA: {0}, {1}. minMaxB: {2}, {3}", minMax.x, minMax.y, minMaxB.x, minMaxB.y));
			if(minMaxB.y < minMax.x || minMax.y < minMaxB.x){
				Debug.Log("No Collision!");
				return false;
			}
		}

		return true;
	}

	private Vector2 GetMinMax(Vector2[] polyPoints, Vector2 axis){
		Vector2 minMax = new Vector2 (float.MaxValue, float.MinValue);
		for (int i=0; i<polyPoints.Length; i++) {
			float dotProd = Vector2.Dot(polyPoints[i], axis);
			if(dotProd < minMax.x) minMax.x = dotProd;
			if(minMax.y < dotProd) minMax.y = dotProd;
		}
		return minMax;
	}

	//Returns the unit vector perpendicular to the provided one
	private static Vector2 GetNormalPerp(Vector2 deltaVect){
		Vector2 returnVect = new Vector2 (deltaVect.y, deltaVect.x).normalized;
		return returnVect;
	}
}
