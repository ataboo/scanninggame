using UnityEngine;
using System;

//Serializable version of Vector2 with float precision
//Needed serializable Vector2 to hold values of SpaceObject, etc.
[Serializable]
public class FloatVect2{
	public float x;
	public float y;
	public static FloatVect2 zero = new FloatVect2(0,0);

	public static float Dot(FloatVect2 v1, FloatVect2 v2){
		return v1.DotProd(v2);
	}

	public static FloatVect2[] ConvertArray(Vector2[] v2Arr){
		FloatVect2[] retArr = new FloatVect2[v2Arr.Length];
		for (int i=0; i<retArr.Length; i++){
			retArr[i] = new FloatVect2(v2Arr[i].x, v2Arr[i].y);
		}
		
		return retArr;
	}

	public FloatVect2(float xVal, float yVal){
		this.x = xVal;
		this.y = yVal;
	}

	public FloatVect2(){
		this.x = 0f;
		this.y = 0f;
	}

	public FloatVect2(double xVal, double yVal){
		this.x = (float) xVal;
		this.y = (float)yVal;
	}

	public FloatVect2(Vector2 flVect){
		this.x = (float)flVect.x;
		this.y = (float)flVect.y;
	}

	public float Magnitude (){ //pop pop!
		return Mathf.Sqrt (this.MagSqr());
	}
	
	public FloatVect2 Normalized (){
		float mag = this.Magnitude ();
		if (mag > 0) {
			return new FloatVect2 (x / mag, y / mag);
		} else {
			return new FloatVect2 (0, 0);
		}
	}
	
	public float MagSqr(){
		return (x * x + y * y);
	}
	
	public float DotProd(FloatVect2 vect2){
		return this.x * vect2.x + this.y * vect2.y;
	}
	
	public float DotProd(Vector2 vect2){
		return (float) this.x * vect2.x + this.y * vect2.y;
	}
	
	//Returns Vector perpendicular clockwise
	public FloatVect2 PerpVector(){
		return new FloatVect2 (this.y, -this.x);
	}
	
	public Vector3 ToFlVector3(){
		return new Vector3 (this.x, this.y, 0);
	}
	
	public Vector3 ToFlVector3(float zVal){
		return new Vector3 (this.x, this.y, zVal);
	}
	
	public Vector2 ToFlVector2(){
		return new Vector2(this.x, this.y);
	}

	public DoubleVect2 ToDoubleVect2(){
		return new DoubleVect2((double) this.x, (double) this.y);
	}
	
	//Rotates clockwise
	public FloatVect2 Rotate(float thetaClock){
		thetaClock = -(float) MathUtils.degToRad (thetaClock);
		float cosTheta = Mathf.Cos (thetaClock);
		float sinTheta = Mathf.Sin (thetaClock);
		return new FloatVect2(this.x * cosTheta - this.y * sinTheta, this.x * sinTheta + this.y * cosTheta);
	}
	
	public float EulerAngle(){
		return (float) MathUtils.radToDeg(Math.Atan2 (this.x, this.y));
	}
	
	public float CrossProd(FloatVect2 crossVect){
		return this.x * crossVect.y - this.y * crossVect.x;
	}

	//==============================Static Functions===============================

	public static FloatVect2 operator +(FloatVect2 v1, FloatVect2 v2){
		return new FloatVect2 (v1.x + v2.x, v1.y + v2.y);
	}

	public static FloatVect2 operator +(FloatVect2 v1, Vector2 v2){
		return new FloatVect2 (v1.x + v2.x, v1.y + v2.y);
	}

	public static FloatVect2 operator -(FloatVect2 v1, FloatVect2 v2){
		return new FloatVect2 (v1.x - v2.x, v1.y - v2.y);
	}

	public static FloatVect2 operator -(FloatVect2 v1, Vector2 v2){
		return new FloatVect2 (v1.x - v2.x, v1.y - v2.y);
	}

	public static FloatVect2 operator *(FloatVect2 v1, float factor){
		return new FloatVect2 (v1.x * factor, v1.y * factor);
	}

	public static FloatVect2 operator *(FloatVect2 v1, double factor){
		return new FloatVect2 (v1.x * factor, v1.y * factor);
	}

	public static FloatVect2 operator /(FloatVect2 v1, float factor){
		return new FloatVect2 (v1.x / factor, v1.y / factor);
	}
	
	public static FloatVect2 operator /(FloatVect2 v1, double factor){
		return new FloatVect2 (v1.x / factor, v1.y / factor);
	}
	
	public static FloatVect2 UnitVectFromAngle(float theta){
		theta = (float) MathUtils.degToRad (theta);
		return new FloatVect2(Math.Sin (theta), Math.Cos(theta));
	}


	
}
