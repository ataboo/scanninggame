using UnityEngine;
using System;

//Serializable version of Vector 2 with Double precision.
[Serializable]
public class DoubleVect2{
	public double x;
	public double y;
	public static DoubleVect2 zero = new DoubleVect2(0,0);

	public DoubleVect2(double xVal, double yVal){
		this.x = xVal;
		this.y = yVal;
	}

	public DoubleVect2(){
		this.x = 0d;
		this.y = 0d;
	}

	public DoubleVect2(float xVal, float yVal){
		this.x = (double) xVal;
		this.y = (double)yVal;
	}

	public DoubleVect2(Vector2 flVect){
		this.x = (double)flVect.x;
		this.y = (double)flVect.y;
	}

	public double Magnitude (){ //pop pop!
		return MathUtils.sqrt (this.MagSqr());
	}
	
	public DoubleVect2 Normalized (){
		double mag = this.Magnitude ();
		if (mag > 0) {
			return new DoubleVect2 (x / mag, y / mag);
		} else {
			return new DoubleVect2 (0, 0);
		}
	}
	
	public double MagSqr(){
		return (x * x + y * y);
	}
	
	public double DotProd(DoubleVect2 vect2){
		return this.x * vect2.x + this.y * vect2.y;
	}
	
	public float DotProd(Vector2 vect2){
		return (float) this.x * vect2.x + (float) this.y * vect2.y;
	}
	
	//Returns Vector perpendicular clockwise
	public DoubleVect2 PerpVector(){
		return new DoubleVect2 (this.y, -this.x);
	}
	
	public Vector3 ToFlVector3(){
		return new Vector3 ((float) this.x, (float) this.y, 0);
	}
	
	public Vector3 ToFlVector3(float zVal){
		return new Vector3 ((float)this.x, (float)this.y, zVal);
	}
	
	public Vector2 ToFlVector2(){
		return new Vector2((float) this.x, (float) this.y);
	}

	public FloatVect2 ToFloatVect2(){
		return new FloatVect2 (this.x, this.y);
	}
	
	//Rotates clockwise
	public DoubleVect2 Rotate(float thetaClock){
		thetaClock = -(float) MathUtils.degToRad (thetaClock);
		double cosTheta = Mathf.Cos (thetaClock);
		double sinTheta = Mathf.Sin (thetaClock);
		return new DoubleVect2(this.x * cosTheta - this.y * sinTheta, this.x * sinTheta + this.y * cosTheta);
	}
	
	public float EulerAngle(){
		return (float) MathUtils.radToDeg(Math.Atan2 (this.x, this.y));
	}
	
	public double CrossProd(DoubleVect2 crossVect){
		return this.x * crossVect.y - this.y * crossVect.x;
	}

	//==============================Static Functions===============================

	public static DoubleVect2 operator +(DoubleVect2 v1, DoubleVect2 v2){
		return new DoubleVect2 (v1.x + v2.x, v1.y + v2.y);
	}

	public static DoubleVect2 operator +(DoubleVect2 v1, Vector2 v2){
		return new DoubleVect2 (v1.x + v2.x, v1.y + v2.y);
	}

	public static DoubleVect2 operator -(DoubleVect2 v1, DoubleVect2 v2){
		return new DoubleVect2 (v1.x - v2.x, v1.y - v2.y);
	}

	public static DoubleVect2 operator -(DoubleVect2 v1, Vector2 v2){
		return new DoubleVect2 (v1.x - v2.x, v1.y - v2.y);
	}

	public static DoubleVect2 operator *(DoubleVect2 v1, float factor){
		return new DoubleVect2 (v1.x * factor, v1.y * factor);
	}

	public static DoubleVect2 operator *(DoubleVect2 v1, double factor){
		return new DoubleVect2 (v1.x * factor, v1.y * factor);
	}

	public static DoubleVect2 operator /(DoubleVect2 v1, float factor){
		return new DoubleVect2 (v1.x / factor, v1.y / factor);
	}
	
	public static DoubleVect2 operator /(DoubleVect2 v1, double factor){
		return new DoubleVect2 (v1.x / factor, v1.y / factor);
	}
	
	public static DoubleVect2 UnitVectFromAngle(float theta){
		theta = (float) MathUtils.degToRad (theta);
		return new DoubleVect2(Math.Sin (theta), Math.Cos(theta));
	}


	
}
