﻿using UnityEngine;
using System.Collections;

public class ScanConeMesh : MonoBehaviour {

	public static ScanConeMesh staticRef;
	private float radius = 10f;
	private static int fanCount = 12; //resolution. needs to be even
	private static float zVal = 0f;
	private static float uvZoom = 1.15f;
	private static float coneAngle = 90f;
	private float oldConeAngle = coneAngle;
	public bool isVisible = false;

	private MeshRenderer meshRenderer;
	private MeshFilter meshFilter;
	public Mesh scanMesh;
	// Use this for initialization
	void Start () {
		staticRef = this;

	}

	private void MakeConeMesh(){
		this.meshRenderer = transform.GetComponent<MeshRenderer> ();
		this.meshFilter = this.transform.GetComponent<MeshFilter> ();

		Vector3[] points = new Vector3[fanCount + 2];
		Vector2[] uvs = new Vector2[points.Length];
		int[] tris = new int[fanCount * 3];

		points [0] = new Vector3 (0, 0, zVal);
		points [1] = new Vector3 (0, radius, zVal);
		points [2] = PositionFromAngle (radius, coneAngle / fanCount, zVal);
		points [3] = PositionFromAngle (radius, -coneAngle / fanCount, zVal);

		for (int i=0; i<4; i++) {
			uvs[i] = GetUVCoordinate(points[i]);
		}
		Debug.Log (string.Format ("2: {0}, {1} 3: {2}, {3}", points[2].x, points[2].y, points[3].x, points[3].y));
		new int[]{0,2,1,0,1,3}.CopyTo (tris, 0);

		for (int i=4; i<points.Length; i = i + 2) {
			points[i] = PositionFromAngle(radius, coneAngle * (i/2) / fanCount, zVal);
			points[i+1] = PositionFromAngle(radius, -coneAngle * (i/2) / fanCount, zVal);
			new int[]{0,i,i-2,0,i-1,i+1}.CopyTo(tris, (i - 2) * 3);
			uvs[i] = GetUVCoordinate(points[i]);
			uvs[i+1] = GetUVCoordinate(points[i + 1]);
		}
		this.scanMesh = meshFilter.mesh;
		scanMesh.vertices = points;
		scanMesh.triangles = tris;
		scanMesh.uv = uvs;
		meshFilter.mesh = scanMesh;
	}

	public void UpdateCone(FloatVect2 scanStatsFL, float radius, Color coneColour){
		Vector2 scanStats = scanStatsFL.ToFlVector2 ();
		float rotation  = scanStats.x;
		float widthAngle = scanStats.y;
		this.radius = radius; 

		if (this.scanMesh == null) {
			MakeConeMesh();
		}


		transform.parent.rotation = Quaternion.Euler(new Vector3(0, 180, rotation));
		meshRenderer.material.color = coneColour;
		if (widthAngle == oldConeAngle) {
			return;
		}
		coneAngle = oldConeAngle = widthAngle;

		Debug.Log ("Cone angle changed: moving points");
		Debug.Log (string.Format ("rotation: {0}, width: {1}", scanStats.x, scanStats.y));
		Mesh coneMesh = meshFilter.mesh;
		Vector3[] points = coneMesh.vertices;
		points [1] = PositionFromAngle (radius, 0, zVal);
		for (int i=2; i < points.Length; i= i+2) {
			points[i] = PositionFromAngle(radius, coneAngle * i / 2 / fanCount, zVal);
			points[i+1] = PositionFromAngle(radius, -coneAngle * i / 2 / fanCount, zVal);
		}

		coneMesh.vertices = points;
		meshFilter.mesh = coneMesh;
	}

	private Vector3 PositionFromAngle(float radius, float theta, float zVal){
		float thetaRad = (float)MathUtils.degToRad (MathUtils.clampEuler(theta));
		return new Vector3 (radius * Mathf.Sin (thetaRad), radius * Mathf.Cos (thetaRad), zVal);
	}

	private Vector2 GetUVCoordinate(Vector3 point){

		Vector2 retVect = new Vector2 (point.x, point.y);
		retVect /= 2 * radius * uvZoom;
		retVect += new Vector2 (0.5f, 0.5f);

		//retVect *= uvZoom;
		return retVect;
	}

	public void SetConeVisible(bool isVisible){
		this.isVisible = meshRenderer.enabled = isVisible;
	}


}
