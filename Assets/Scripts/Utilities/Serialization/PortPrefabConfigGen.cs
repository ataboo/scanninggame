using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq.Expressions;

//Attach this to a port prefab to set attributes, hardpoints and collider
public class PortPrefabConfigGen : MonoBehaviour {
	private string prefabName;


	public int submarineSpawnRate = 3;
	public int cargoShipSpawnRate = 3;
	public int tankerSpawnRate = 5;
	private int[] spawnRates;
	//TODO: Scanner as attached module
	public CollisionTester.ColTypes colliderType = CollisionTester.ColTypes.Circle;

	[HideInInspector]
	public HardPoint[] hardPoints;
	private FloatVect2[] colliderPoints;

	void Start(){
		this.prefabName = transform.name;

		this.spawnRates = new int[]{submarineSpawnRate, cargoShipSpawnRate, tankerSpawnRate, 0};
		if (spawnRates.Length != (int)ConfigParser.SpaceObjectTypes.Count) {
			Debug.LogError ("spawnRates length doesn't match PhysicsManager.SpaceTypes.Count");
		}

		switch (colliderType) {
		case CollisionTester.ColTypes.Box:
			BoxCollider2D boxCollide = transform.GetComponentInChildren<BoxCollider2D>();
			if(boxCollide == null){
				Debug.LogError("No Box collider found on prefab " + prefabName + " despite one being specified");
				return;
			}
			float xVal = boxCollide.size.x/2;
			float yVal = boxCollide.size.y/2;
			this.colliderPoints = new FloatVect2[]{new FloatVect2(xVal, yVal), new FloatVect2(xVal, -yVal),
				new FloatVect2(-xVal, -yVal), new FloatVect2(-xVal, yVal)};
			break;

		case CollisionTester.ColTypes.Poly:
			PolygonCollider2D polyCollide = transform.GetComponentInChildren<PolygonCollider2D> ();
			if(polyCollide == null){
				Debug.LogError("No Polygon collider found on prefab " + prefabName + " despite one being specified");
				return;
			}
			this.colliderPoints = FloatVect2.ConvertArray(polyCollide.points);
			break;
		case CollisionTester.ColTypes.Circle:
			CircleCollider2D circCollide = transform.GetComponentInChildren<CircleCollider2D>();
			if(circCollide == null){
				Debug.LogError("No Circle collider found on prefab " + prefabName + " despite one being specified");
				return;
			}
			this.colliderPoints = new FloatVect2[]{new FloatVect2(circCollide.offset), new FloatVect2(circCollide.radius, 0)};
			break;
		default: //point
			this.colliderPoints = new FloatVect2[]{new FloatVect2(0,0)};
			break;
		}

		int hardPointCount = 0;
		int weaponCount = 0;
		List<HardPoint> hPointList = new List<HardPoint> ();
		foreach (Transform child in transform) {
			if(child.tag.Equals("hard_point")){
				HardPoint hardPoint = HardPoint.MakeHardPoint(child, hardPointCount, prefabName);
				hPointList.Add (hardPoint);
				if(hardPoint.pointType == HardPoint.PointType.Weapon)
					weaponCount++;
				hardPointCount++;
			}
		}
		this.hardPoints = hPointList.ToArray();

		PortSpawner prefabPort = new PortSpawner ();
		CopyPortVariables (prefabPort, new int[]{weaponCount});
		CollisionTester.SetMinColRadSqr (prefabPort);
		FileSerializer.SaveClassToXML (prefabPort.prefabName, prefabPort);
		FileSerializer.SaveClassToBin (prefabPort.prefabName, prefabPort);
	}

	private void CopyPortVariables(PortSpawner portSpawner, int[] pointTypeCounts){
		portSpawner.prefabName = this.prefabName;
		portSpawner.className = PortSpawner._className;

		//portSpawner.weapons = new ShipModule[pointTypeCounts[1]];
		portSpawner.hardPoints = this.hardPoints;
		portSpawner.hitBoxPoints = colliderPoints;
		portSpawner.colliderType = colliderType;
		portSpawner.spawnProbs = spawnRates;
		//portSpawner.weapons = new ShipModule[pointTypeCounts[0]];

		foreach (HardPoint hardPoint in hardPoints) {
			if(hardPoint.pointType == HardPoint.PointType.Dock){
				if((hardPoint.dockType & HardPoint.DockType.InDock) == HardPoint.DockType.InDock){
					portSpawner.inDockOffset = hardPoint.localPosition;
				}
				if((hardPoint.dockType & HardPoint.DockType.OutDock) == HardPoint.DockType.OutDock){
					portSpawner.outDockOffset = hardPoint.localPosition;
				}
				continue;
			}
			//hardPoint.AddDefaultModule(portSpawner);
		}
	}






}




