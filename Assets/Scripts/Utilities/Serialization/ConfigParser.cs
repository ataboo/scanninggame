﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Collections.Generic;

//Would make this static but this way can just load once
public static class ConfigParser {
	public enum SpaceObjectTypes {Submarine, CargoShip, Tanker, PortSpawner, Count};
	public static string[] prefabNames = {"SubPrefab", "CargoPrefab", "TankerPrefab", "PortPrefab"};
	
	public static Vessel MakeVesselFromConfig(SpaceObjectTypes type, string name, DoubleVect2 position){
		if (type == SpaceObjectTypes.PortSpawner) {
			Debug.LogError("Port Spawner isn't a valid Vessel Type");
			return null;
		}

		Vessel vessel = ParseVessel(type);
		vessel.name = name;
		vessel.position = position;
		return vessel;
	}

	public static PortSpawner MakePortFromConfig(string name, DoubleVect2 position, float heading){
		PortSpawner pSpawn = ParsePort();
		pSpawn.name = name;
		pSpawn.position = position;
		pSpawn.heading = heading;
		pSpawn.SetupDocks ();
		return pSpawn;
	}

	private static Vessel ParseVessel(SpaceObjectTypes type){
		string prefabName = prefabNames [(int)type];
		//Vessel vessel = (Vessel) FileSerializer.LoadClassXml (prefabName, typeof(Vessel));
		return (Vessel)FileSerializer.LoadClass (prefabName, typeof(Vessel));
	}
	
	private static PortSpawner ParsePort(){
		string prefabName = prefabNames [(int)SpaceObjectTypes.PortSpawner];
		//PortSpawner port = (PortSpawner) FileSerializer.LoadClassXml(prefabName, typeof(PortSpawner));
		return (PortSpawner)FileSerializer.LoadClass(prefabName, typeof(PortSpawner));
	}

	public static Weapon MakeWeaponFromConfig(Weapon.WeaponPrefabs type){
		string prefabName = Weapon.weaponPrefabNames [(int)type];
		return (Weapon)FileSerializer.LoadClass (prefabName, typeof(Weapon));
	}
}


