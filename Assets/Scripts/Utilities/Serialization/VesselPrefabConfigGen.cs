﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq.Expressions;

//Attach this to a vessel prefab and set attributes, hardpoints and collider info
public class VesselPrefabConfigGen : MonoBehaviour {
	private string prefabName;
	public string className = "";
	

	//TODO: Scanner as attached module
	public CollisionTester.ColTypes colliderType;

	[Header("Scanner Stats:")]
	public FloatVect2 scanRanges = new FloatVect2(3000f, 10000f);
	public FloatVect2 scanSpreads = new FloatVect2 (3f, 90f);
	public float scanSlewRate = 30f;
	
	[Header("Movement Stats:")]
	//Vessel Attributes
	public FloatVect2 speedLimits = new FloatVect2 (-50, 100);
	public float enginePower = 8f; //Maximum engine acceleration.  Will change with damage, etc. also effects restore force
	public float keelFactor = 0.5f; //Restoring lateral force as factor of engine power
	public float reverseThrust = 0.5f;
	public float maxTurnRate = 30f;
	public float courseDelta = 0.1f; //Rate of player changing course
	public float throttleDelta = 1f;  //Rate of player changing throttle
	public float waypointThreshold = 20f; //Distance to waypoint before cleared

	[HideInInspector]
	public HardPoint[] hardPoints;
	public FloatVect2[] colliderPoints;

	void Start(){
		this.prefabName = transform.name;

		switch (colliderType) {
		case CollisionTester.ColTypes.Box:
			BoxCollider2D boxCollide = transform.GetComponentInChildren<BoxCollider2D>();
			if(boxCollide == null){
				Debug.LogError("No Box collider found on prefab " + prefabName + " despite one being specified");
				return;
			}
			float xVal = boxCollide.size.x/2;
			float yVal = boxCollide.size.y/2;
			this.colliderPoints = new FloatVect2[]{new FloatVect2(xVal, yVal), new FloatVect2(xVal, -yVal),
				new FloatVect2(-xVal, -yVal), new FloatVect2(-xVal, yVal)};
			break;

		case CollisionTester.ColTypes.Poly:
			PolygonCollider2D polyCollide = transform.GetComponent<PolygonCollider2D> ();
			if(polyCollide == null){
				Debug.LogError("No Polygon collider found on prefab " + prefabName + " despite one being specified");
				return;
			}
			this.colliderPoints = FloatVect2.ConvertArray(polyCollide.points);
			break;
		case CollisionTester.ColTypes.Circle:
			CircleCollider2D circCollide = transform.GetComponentInChildren<CircleCollider2D>();
			if(circCollide == null){
				Debug.LogError("No Circle collider found on prefab " + prefabName + " despite one being specified");
				return;
			}
			this.colliderPoints = new FloatVect2[]{new FloatVect2(circCollide.offset), new FloatVect2(circCollide.radius, 0)};
			break;
		default: //point
			this.colliderPoints = new FloatVect2[]{new FloatVect2(0,0)};
			break;
		}

		int hardPointCount = 0;
		int thrusterCount = 0;
		int weaponCount = 0;
		List<HardPoint> hPointList = new List<HardPoint> ();
		foreach (Transform child in transform) {
			if(child.tag.Equals("hard_point")){
				HardPoint hardPoint = HardPoint.MakeHardPoint(child, hardPointCount, prefabName);
				hPointList.Add(hardPoint);
				if(hardPoint.pointType == HardPoint.PointType.Thruster)
					thrusterCount++;
				if(hardPoint.pointType == HardPoint.PointType.Weapon)
					weaponCount++;
				hardPointCount++;
			}
		}
		this.hardPoints = hPointList.ToArray();

		Vessel prefabVessel = new Vessel ();
		CopyVesselVariables (prefabVessel, new int[]{thrusterCount, weaponCount});
		CollisionTester.SetMinColRadSqr (prefabVessel);
		FileSerializer.SaveClassToXML (prefabVessel.prefabName, prefabVessel);
		FileSerializer.SaveClassToBin (prefabVessel.prefabName, prefabVessel);
	}



	private void CopyVesselVariables(Vessel vessel, int[] pointTypeCounts){
		vessel.prefabName = this.prefabName;
		vessel.className = this.className;
		vessel.scanSlewRate = this.scanSlewRate;
		vessel.enginePower = this.enginePower;
		vessel.keelFactor = this.keelFactor;
		vessel.reverseThrust = this.reverseThrust;
		vessel.maxTurnRate = this.maxTurnRate;
		vessel.courseDelta = this.courseDelta;
		vessel.throttleDelta = this.throttleDelta;
		vessel.waypointThreshold = this.waypointThreshold;
		vessel.scanSpreads = this.scanSpreads;
		vessel.scanRanges = this.scanRanges;
		vessel.speedLimits = this.speedLimits;
		
		vessel.thrusters = new Thruster[pointTypeCounts[0]];
		vessel.weapons = new Weapon[pointTypeCounts[1]];
		vessel.hardPoints = this.hardPoints;
		vessel.hitBoxPoints = this.colliderPoints;
		vessel.colliderType = colliderType;

		foreach (HardPoint hardPoint in hardPoints) {
			hardPoint.AddDefaultModule(vessel);
		}
	}

	/*
	private void SerializeToXML(Vessel vessel){
		XmlSerializer serializer = new XmlSerializer (typeof(Vessel));
		
		if (!System.IO.Directory.Exists (outputPath)) {
			System.IO.Directory.CreateDirectory (outputPath);
		}
		FileStream stream = new FileStream (outputPath + prefabName + outputSuffix, FileMode.Create);
		serializer.Serialize (stream, vessel);
		Debug.Log (string.Format("Wrote vessel config to: " + outputPath + prefabName + outputSuffix));
		stream.Close ();
	}
	*/


}




