﻿using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

public class FileSerializer {

	public static readonly string dirBin = "C:/Scan2DConfigs/Serialized/";
	public static readonly string suffixBin = "_config.bytes";

	public static readonly string dirXml = "C:/Scan2dConfigs/Serialized/";
	public static readonly string suffixXml = "_config.xml";

	public static readonly bool xmlModeOn = false;

	public static object LoadClass(string prefabName, System.Type objType){
		if(xmlModeOn){
			return LoadClassXml(prefabName, objType);
		}
		return LoadClassFromBin(prefabName);
	}

	public static void BinSaveToFile(string fileName, object obj){
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		if (!System.IO.Directory.Exists (dirBin)) {
			System.IO.Directory.CreateDirectory (dirBin);
		}
		
		FileStream fileStream = new FileStream(dirBin + fileName, FileMode.Create);
		binaryFormatter.Serialize (fileStream, obj);
		
	}

	public static object BinLoadFromFile(string filePath){
		FileStream fileStream = new FileStream (filePath, FileMode.Open);

		BinaryFormatter binaryFormatter = new BinaryFormatter ();
		return binaryFormatter.Deserialize (fileStream);
	}

	public static void SaveClassToXML(string prefabName, object saveObject){
		XmlSerializer serializer = new XmlSerializer (saveObject.GetType());
		if (!System.IO.Directory.Exists (dirXml)) {
			System.IO.Directory.CreateDirectory (dirXml);
		}
		FileStream stream = new FileStream (dirXml + prefabName + suffixXml, FileMode.Create);
		serializer.Serialize (stream, saveObject);
		Debug.Log (string.Format("Wrote config to: " + dirXml + prefabName + suffixXml));
		stream.Close ();
	}

	private static object LoadClassXml(string prefabName, System.Type objType){
		XmlSerializer serializer = new XmlSerializer (objType);
		TextAsset prefabText = Resources.Load(prefabName + "_config") as TextAsset;
		if (prefabText == null) {
			Debug.LogError("Can't find prefab config file: " + prefabName);
		}
		
		StringReader reader = new StringReader (prefabText.text);
		object retObj = serializer.Deserialize (reader);
		if (retObj == null) {
			Debug.LogError ("Something went wrong with deserializing prefab config " + prefabName);
		}
		return retObj;
	}

	private static object LoadClassFromBin(string prefabName){
		TextAsset prefabText = Resources.Load(prefabName + "_config") as TextAsset;
		if (prefabText == null) {
			Debug.LogError("Can't find prefab config file: " + prefabName + "_config");
		}
		MemoryStream memoryStream = new MemoryStream (prefabText.bytes);
		BinaryFormatter binaryFormatter = new BinaryFormatter ();
		return binaryFormatter.Deserialize (memoryStream);
	}

	public static void SaveClassToBin(string prefabName, object classObject){
		BinSaveToFile (prefabName + suffixBin, classObject);
	}
	
}
