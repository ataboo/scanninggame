﻿using UnityEngine;
using System.Collections;
using Vectrosity;


public static class CollisionTester  {
	public enum ColTypes{Box, Poly, Circle, Point};

	// Update is called once per frame
	public static bool RunSAT (SpaceObject objA, SpaceObject objB) {
		if (!LightCircleCircle(objA, objB))
			return false;
		Debug.Log (string.Format ("objA: {0} serial {1} and objB: {2} serial {3} are close enough to test collision further", 
				objA.className, objA.serialNumber, objB.className, objB.serialNumber));

		if((objA.colliderType == ColTypes.Circle || objA.colliderType == ColTypes.Point) &&
		   (objB.colliderType == ColTypes.Circle || objB.colliderType == ColTypes.Point)){
			if(!TightCircleCircle(objA, objB)){  //There's redundancy here
				return false;
			}
			Debug.Log (string.Format("objA: {0} serial {1} and objB: {2} serial {2} collided as circles or points", 
					objA.className, objA.serialNumber, objB.className, objB.serialNumber));
			return true;
		}

		//Should pre calculate hitbox points for static objects
		if ((objA.colliderType == ColTypes.Box || objA.colliderType == ColTypes.Poly) &&
				(objB.colliderType == ColTypes.Box || objB.colliderType == ColTypes.Poly)) {
			bool polyPoly = PolyPolySAT(objA, objB);
			if(polyPoly)	
				Debug.Log (string.Format("Poly-Poly Collision Detected! between {0} {1} and {2} {3}", 
					objA.className, objA.serialNumber, objB.className, objB.serialNumber));
			return polyPoly;
		}

		bool polyCircle =  PolyCircPointSAT (objA, objB);
		if(polyCircle)
			Debug.Log (string.Format("Poly-Circle Collision Detected! between {0} {1} and {2} {3}", 
		              objA.className, objA.serialNumber, objB.className, objB.serialNumber));
		return polyCircle;

	}

	public static void SetMinColRadSqr(SpaceObject spaceObj){
		if (spaceObj.colliderType == ColTypes.Point) {
			spaceObj.minColliderRadSqr = 0f;
			return;
		}
		
		if (spaceObj.hitBoxPoints == null) {
			Debug.LogError(string.Format("No hitBoxPoints on {0} serial {1}.", spaceObj.className, spaceObj.serialNumber));
			return;
		}
		
		if (spaceObj.colliderType == ColTypes.Circle) {
			spaceObj.minColliderRadSqr = spaceObj.hitBoxPoints[1].x * spaceObj.hitBoxPoints[1].x;
			return;
		}
		//Assume poly or box
		foreach(FloatVect2 point in spaceObj.hitBoxPoints){
			float sqrMag = point.MagSqr();
			if(sqrMag > spaceObj.minColliderRadSqr) spaceObj.minColliderRadSqr = sqrMag;
		}
	}

	private static bool LightCircleCircle(SpaceObject objA, SpaceObject objB){
		double distSqr = (objA.position - objB.position).MagSqr ();
		double radSqrs = ((double)objA.minColliderRadSqr + (double)objB.minColliderRadSqr) * 1.2;
		//Debug.Log (string.Format ("objA: {0} serial {1} and objB: {2} serial {3}. distSqr: {4}, radSqrs: {5}", 
		                          //objA.className, objA.serialNumber, objB.className, objB.serialNumber, distSqr, radSqrs));

		return distSqr < radSqrs;
	}

	private static bool TightCircleCircle(SpaceObject objA, SpaceObject objB){
		//Maybe do dist instead of sqr down the road?
		double distSqr = (objA.position - objB.position).MagSqr ();
		double radSqrs = ((double)objA.minColliderRadSqr + (double)objB.minColliderRadSqr);

		return distSqr < radSqrs;
	}

	private static bool PolyPolySAT(SpaceObject objA, SpaceObject objB){
		FloatVect2[] polyPointsA = TranslatePolyPoints(objA.hitBoxPoints, DoubleVect2.zero, objA.heading);
		FloatVect2[] polyPointsB = TranslatePolyPoints(objB.hitBoxPoints, objB.position - objA.position, objB.heading);
		
		bool boxAxes = objA.colliderType == ColTypes.Box;
		if (!TestPolyAxes (polyPointsA, polyPointsB, boxAxes)) {
			//Debug.Log ("Gap Found A --> B");
			return false;
		}
		
		if (!TestPolyAxes (polyPointsB, polyPointsA, boxAxes)) {
			//Debug.Log ("Gap Found B--> A");
			return false;
		}

		return true;
	}
	
	private static bool PolyCircPointSAT(SpaceObject objA, SpaceObject objB){
		SpaceObject polyObj = objB;
		SpaceObject circleObj = objA;

		if (objA.colliderType == ColTypes.Box || objA.colliderType == ColTypes.Poly) {
			circleObj = objB;
			polyObj = objA;
		}

		if (!(polyObj.colliderType == ColTypes.Box || polyObj.colliderType == ColTypes.Poly)) {
			Debug.LogError(string.Format("Invalid poly/box --> circle/point collision objects.  " +
					"Expected {0} serial {1} to be box or poly collider.", polyObj.className, polyObj.serialNumber));
			return false;
		}

		if (!(circleObj.colliderType == ColTypes.Circle || circleObj.colliderType == ColTypes.Point)) {
			Debug.LogError(string.Format("Invalid poly/box --> circle/point collision objects.  " +
			                             "Expected {0} serial {1} to be box or poly collider.", circleObj.className, circleObj.serialNumber));
			return false;
		}

		FloatVect2[] polyPoints = TranslatePolyPoints (polyObj.hitBoxPoints, DoubleVect2.zero, polyObj.heading);
		FloatVect2 circleDeltaPos = (circleObj.position - polyObj.position).ToFloatVect2();
		float radius = 0;
		if (circleObj.colliderType == ColTypes.Circle) {
			circleDeltaPos += circleObj.hitBoxPoints[0];
			radius = circleObj.hitBoxPoints[1].x;
		}
		float radSqr = radius * radius;
		//Vector3 circlePoints = new Vector3 (circleDeltaPos.x, circleDeltaPos.y, radius);

		int closestIndex = 0;
		float closestDistSqr = float.MaxValue;
		for(int i=0; i< polyPoints.Length; i++){
			float newDistSqr =  (polyPoints[i] - circleDeltaPos).MagSqr();
			if(newDistSqr < radSqr)
				return true;
			if(newDistSqr < closestDistSqr){
				closestIndex = i;
				closestDistSqr = newDistSqr;
			}
		}
		FloatVect2 p0 = polyPoints [closestIndex];
		FloatVect2 pRight = (closestIndex == polyPoints.Length-1) ? polyPoints[0]: polyPoints[closestIndex + 1];
		FloatVect2 pLeft = (closestIndex == 0) ? polyPoints[polyPoints.Length-1]: polyPoints[closestIndex - 1];

		int leftEdge = CheckEdge (polyPoints, pRight, p0, circleDeltaPos, radius);
		if(leftEdge >= 0){
			//Debug.Log("Left edge difinitive at " + leftEdge);
			return leftEdge == 1;
		}
		int rightEdge = CheckEdge (polyPoints, p0, pLeft, circleDeltaPos, radius);
		//Debug.Log ("Right edge returned " + rightEdge);
		return rightEdge == 1;
	}

	public static bool PathCheck(DoubleVect2[] pathPoints, float pathWidth, SpaceObject obj){
		DoubleVect2 path = pathPoints[0] - pathPoints[1];
		return false;
	}

	private static bool TestPolyAxes(FloatVect2[] polyPointsA, FloatVect2[] polyPointsB, bool boxCollide){
		for (int i=0; i<polyPointsA.Length; i++) {
			if(boxCollide && i == 2) continue; //Only test 2 axes for box collider (square)
			FloatVect2 p0 = polyPointsA[i];
			FloatVect2 p1 = (i < polyPointsA.Length -1) ? polyPointsA[i+1]: polyPointsA[0];
			//Debug.Log (string.Format ("Point {0} is {1}, {2}", i, polyPointsA[i].x, polyPointsA[i].y));
			
			
			//Get Normal axis from edge
			FloatVect2 normPerp = GetNormalPerp(p0 - p1);
			FloatVect2 minMaxA = GetMinMax(polyPointsA, normPerp);
			FloatVect2 minMaxB = GetMinMax(polyPointsB, normPerp);
			

			//Debug.Log(string.Format("i={4} minMaxA: {0}, {1}. minMaxB: {2}, {3}", minMaxA.x, minMaxA.y, minMaxB.x, minMaxB.y, i));
			//Debug.Log ("check is: " + (minMaxB.y < minMaxA.x || minMaxA.y < minMaxB.x));

			if(minMaxB.y < minMaxA.x || minMaxA.y < minMaxB.x){
				//Debug.Log ("No Collision");
				return false;
			}
		}
		return true;
	}

	private static FloatVect2[] TranslatePolyPoints(FloatVect2[] flVects, DoubleVect2 centerPos, float heading){
		FloatVect2[] retVects = new FloatVect2[flVects.Length];
		for (int i=0; i<flVects.Length; i++) {
			//retVects[i] = MathUtils.RotateVect(flVects[i], heading);
			retVects[i] = flVects[i].Rotate(heading);
			retVects[i] = retVects[i] + centerPos.ToFloatVect2();
		}
		return retVects;
	}

	private static FloatVect2 GetMinMax(FloatVect2[] polyPoints, FloatVect2 axis){
		FloatVect2 minMax = new FloatVect2 (float.MaxValue, float.MinValue);
		for (int i=0; i<polyPoints.Length; i++) {
			float dotProd = FloatVect2.Dot (polyPoints[i], axis);
			if(dotProd < minMax.x) minMax.x = dotProd;
			if(minMax.y < dotProd) minMax.y = dotProd;
		}
		return minMax;
	}

	//Returns the unit vector perpendicular to the provided one
	private static FloatVect2 GetNormalPerp(FloatVect2 deltaVect){
		FloatVect2 norm = deltaVect.Normalized();
		FloatVect2 normPerp = new FloatVect2(norm.y, -norm.x);
		//Debug.Log (string.Format("deltaVect: {0}, {1}, normPerp: {2}, {3}", deltaVect.x, deltaVect.y, normPerp.x, normPerp.y));
		return normPerp;
	}

	private static int CheckEdge(FloatVect2[] polyPoints, FloatVect2 p0, FloatVect2 p1, FloatVect2 testPoint, float radius){
		FloatVect2 testUnit = (p1 - p0).Normalized();
		float testParDot = FloatVect2.Dot (testPoint, testUnit);

		FloatVect2 parMinMax = new FloatVect2 (FloatVect2.Dot (p0, testUnit), FloatVect2.Dot (p1, testUnit));
		//Debug.Log (string.Format ("testParDot {0}, parMin {1} parMax{2}", testParDot, parMinMax.x, parMinMax.y));
		if (testParDot < parMinMax.x || parMinMax.y < testParDot) {
			return -1; //Point isn't in this Voronoi.  Not definitive on collision
		}

		FloatVect2 testUnitPerp = new FloatVect2(testUnit.y, -testUnit.x);  //PFM why -y points the proper way 
		float testPerpDot = FloatVect2.Dot (testPoint, testUnitPerp);
		float perpMin = FloatVect2.Dot (p0, testUnitPerp);
		//Debug.Log (string.Format ("testPerpDot {0}, perpMin + radius {1}", testPerpDot, perpMin + radius));
		return (testPerpDot < perpMin + radius) ? 1: 0;  //Difinitive collision or not
	}
	
}
